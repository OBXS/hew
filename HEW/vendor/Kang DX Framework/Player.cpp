#include "Player.h"

Player::Player() {
	TextureManager::Load("Sample.png", Vector2(189.f, 250.f));
	sprite = new Sprite("Sample", this);

	collider = Collision2DSystem::PickOut(ColliderType::Player)->Init(Collider2DShape::Circle, Vector2(200.f, 200.f), this);

	anim = TweenSystem::GetSequence()->SetLoop(-1);
	anim->AddInEnd(TweenSystem::MoveLocalYTo(sprite, 15.f, 1.f)->SetEase(Ease::InQuad));
	anim->AddInEnd(TweenSystem::MoveLocalYTo(sprite, 0.f, 1.f));

	speed = 200.f;
}

Player::~Player() {
	delete sprite;
}

void Player::Update() {
	// Move
	Vector2 v = Vector2(0.f, 0.f);

	if (Input::IsKeyPressed('W') || Input::IsKeyPressed(VK_UP)) v.y = 1.f;
	else if (Input::IsKeyPressed('S') || Input::IsKeyPressed(VK_DOWN)) v.y = -1.f;
	if (Input::IsKeyPressed('A') || Input::IsKeyPressed(VK_LEFT)) v.x = -1.f;
	else if (Input::IsKeyPressed('D') || Input::IsKeyPressed(VK_RIGHT)) v.x = 1.f;

	D3DXVec2Normalize(&v, &v);
	Move(v * speed * Time::GetDeltaTime());
}

void Player::Draw() {
	sprite->Draw();
}