#ifndef __SAMPLE_SCENE_H__
#define __SAMPLE_SCENE_H__

#include "System\Scene.h"
#include "System\Camera.h"
#include "Player.h"

class SampleScene : public Scene {
public:
	SampleScene(Camera *camera);
	~SampleScene();

	void Enter();
	void Update();
	void Draw();
	void Exit();

private:
	Camera *camera;
	Player *player;
};

#endif