#include "System\SceneManager.h"
#include "System\ScreenFader.h"
#include "SampleScene.h"

static Camera *camera = nullptr;

void InitGame() {
	Collision2DSystem::DisableChackTableExceptDefault();
	Collision2DSystem::SetChackTable(ColliderType::Input, ColliderType::UI, true, false);
	camera = new Camera();

	SceneManager::AddScene("Sample", new SampleScene(camera));
	SceneManager::ChangeScene("Sample");
}

void UpdateGame() {
	Collision2DSystem::Update();
	SceneManager::Update();
	TweenSystem::Update();
	camera->Update();
}

void DrawGame() {
	SceneManager::Draw();
	// Draw Colliders
	if (Input::IsKeyPressed('1'))
		Collision2DSystem::DrawShapes();
}