#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "System\Transform.h"

class Player : public Transform {
public:
	Player();
	~Player();

	void Update();
	void Draw();

private:
	Sprite *sprite;
	Collider2D *collider;
	Sequence *anim;

	float speed;
};

#endif