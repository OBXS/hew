#include "Time.h"

double Time::frequency = 0.;
LARGE_INTEGER Time::nowTime;
LARGE_INTEGER Time::lastTime;
float Time::deltaTime = 0.f;

void Time::Init() {
	LARGE_INTEGER time = { 0 };
	QueryPerformanceFrequency(&time);
	frequency = (double)time.QuadPart;
	QueryPerformanceCounter(&lastTime);
}

void Time::WaitFrame() {
	do {
		QueryPerformanceCounter(&nowTime);
		deltaTime = (float)((double)(nowTime.QuadPart - lastTime.QuadPart) / frequency);
	} while (deltaTime < 1.f / 60.f);

	deltaTime = min(deltaTime, 1.f / 20.f);
	QueryPerformanceCounter(&lastTime);
}

float Time::GetTime() {
	QueryPerformanceCounter(&nowTime);
	return (float)((double)nowTime.QuadPart / frequency);
}

float Time::GetDeltaTime() {
	return deltaTime;
}