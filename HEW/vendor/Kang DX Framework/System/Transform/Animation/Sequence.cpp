// Sequence.cpp
// 補間アニメーション序列
//
// Group of the tweeners will play contiuned.
// 連続でプレイする補間アニメーション群。

#include "Sequence.h"

#pragma region Init & Fin & Update

Sequence::Sequence() {
	tweeners = new list<Tweener*>();
	syncTweeners = new list<Tweener*>();
	nowTweener = tweeners->end();
}

Sequence::~Sequence() {
	delete tweeners;
}

void Sequence::Init() {
	nowTweener = tweeners->end();

	isStoped = false;
	isPausing = false;
	hasRewind = false;
	isReverse = false;
	isReverseEnded = false;
	loops = 1;
	loopsNow = 1;
}

void Sequence::AddInEnd(Tweener* tweener) {
	tweener->inSequence = true;
	tweeners->push_back(tweener);
}

void Sequence::AddWithLast(Tweener* tweener) {
	tweener->inSequence = true;
	if (tweeners->size() > 0) {
		tweeners->back()->SetSync(tweener);
		syncTweeners->push_back(tweener);
	} else {
		tweeners->push_back(tweener);
	}
}

void Sequence::Clear() {
	tweeners->clear();
	syncTweeners->clear();
}

list<Tweener*> *Sequence::GetTweeners() {
	tweeners->sort();
	syncTweeners->sort();
	tweeners->merge(*syncTweeners);
	return tweeners;
}

bool Sequence::Update() {
	if (isStoped || tweeners->size() <= 0 || isPausing) return false;
	
	if (nowTweener == tweeners->end())
		nowTweener = tweeners->begin();

	if (!(*nowTweener)->Update()) {
		if (isReverse) {
			if (nowTweener == tweeners->begin()) {
				isReverseEnded = true;
			} else {
				nowTweener--;
			}
		} else {
			nowTweener++;
			if (nowTweener != tweeners->end())
				(*nowTweener)->ResetStart();
		} 
	}

	if (!IsPlaying()) {
		if (loopsNow > 0) {
			loopsNow--;
		} else if (loops < 0) {
			Loop();
			return true;
		}
		if (loopsNow > 0)
			Loop();
	}

	return IsPlaying();
}

// Init & Fin & Update
#pragma endregion

#pragma region State

bool Sequence::IsPlaying() {
	if (isStoped || isPausing || tweeners->size() <= 0 || loopsNow == 0) return false;
	if (isReverse) {
		return !isReverseEnded;
	} else {
		return nowTweener == tweeners->end() ? false : true;
	}
}

bool Sequence::IsEnded() {
	if (isStoped || tweeners->size() <= 0 || loopsNow == 0) return true;
	return false;
}

Sequence *Sequence::Pause() {
	isPausing = true;

	return this;
}

Sequence *Sequence::Play() {
	isPausing = false;

	return this;
}

Sequence *Sequence::End() {
	loopsNow = 1;
	isReverse = false;
	do {
		(*nowTweener)->End();
	} while (Update());
	isStoped = true;

	return this;
}

Sequence *Sequence::Stop() {
	isStoped = true;

	return this;
}

Sequence *Sequence::SetLoop(int loops) {
	this->loops = loops;
	loopsNow = loops;

	return this;
}

Sequence *Sequence::SetRewind(bool hasRewind) {
	this->hasRewind = hasRewind;

	return this;
}

void Sequence::Loop() {
	if (hasRewind) {
		isReverse = !isReverse;
		if (isReverse) {
			for (auto &tweener : *tweeners)
				tweener->Playback();

			nowTweener--;
			return;
		} else {
			isReverseEnded = false;
		}
	}

	for (auto &tweener : *tweeners)
		tweener->Replay();

	nowTweener = tweeners->begin();
}

// State
#pragma endregion