// Tweener.cpp
// 補間アニメーション
//
// Transform animation that set with end and duration, and can set easing.
// 終わりと時間で決めて、緩急も付けれるのトランスフォームアニメーション。

#include "Tweener.h"

#pragma region Init

Tweener *Tweener::Wait(float duration) {
	this->duration = duration;
	startType = StartType::None;
	isWaiting = true;

	return this;
}

Tweener *Tweener::SetCallback(function<void()> callback) {
	this->callback = callback;

	return this;
}

Tweener *Tweener::MoveLocalXTo(Transform *target, float end, float duration) {
	Set(target, Vector3(end, 0.f, 0.f), duration);
	startType = StartType::LocalPosition;
	start = target->GetLocalPosition();
	action = bind(&Tweener::ActMoveLocalXTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::MoveLocalYTo(Transform *target, float end, float duration) {
	Set(target, Vector3(0.f, end, 0.f), duration);
	startType = StartType::LocalPosition;
	start = target->GetLocalPosition();
	action = bind(&Tweener::ActMoveLocalYTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::MoveLocalZTo(Transform *target, float end, float duration) {
	Set(target, Vector3(0.f, 0.f, end), duration);
	startType = StartType::LocalPosition;
	start = target->GetLocalPosition();
	action = bind(&Tweener::ActMoveLocalZTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::MoveLocalTo(Transform *target, Vector2 end, float duration) {
	Set(target, Vector3(end.x, end.y, 0.f), duration);
	startType = StartType::LocalPosition;
	start = target->GetLocalPosition();
	action = bind(&Tweener::ActMoveLocalTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::MoveLocalTo(Transform *target, Vector3 end, float duration) {
	Set(target, end, duration);
	startType = StartType::LocalPosition;
	start = target->GetLocalPosition();
	action = bind(&Tweener::ActMoveLocalTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::MoveXTo(Transform *target, float end, float duration) {
	Set(target, Vector3(end, 0.f, 0.f), duration);
	startType = StartType::Positoin;
	start = target->GetPosition();
	action = bind(&Tweener::ActMoveXTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::MoveYTo(Transform *target, float end, float duration) {
	Set(target, Vector3(0.f, end, 0.f), duration);
	startType = StartType::Positoin;
	start = target->GetPosition();
	action = bind(&Tweener::ActMoveYTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::MoveZTo(Transform *target, float end, float duration) {
	Set(target, Vector3(0.f, 0.f, end), duration);
	startType = StartType::Positoin;
	start = target->GetPosition();
	action = bind(&Tweener::ActMoveZTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::MoveTo(Transform *target, Vector2 end, float duration) {
	Set(target, Vector3(end.x, end.y, 0.f), duration);
	startType = StartType::Positoin;
	start = target->GetPosition();
	action = bind(&Tweener::ActMoveTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::MoveTo(Transform *target, Vector3 end, float duration) {
	Set(target, end, duration);
	startType = StartType::Positoin;
	start = target->GetPosition();
	action = bind(&Tweener::ActMoveTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::RotateLocalXTo(Transform *target, float end, float duration) {
	Set(target, Vector3(end, 0.f, 0.f), duration);
	startType = StartType::LocalRotation;
	start = target->GetLocalRotation();
	action = bind(&Tweener::ActRotateLocalXTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::RotateLocalYTo(Transform *target, float end, float duration) {
	Set(target, Vector3(0.f, end, 0.f), duration);
	startType = StartType::LocalRotation;
	start = target->GetLocalRotation();
	action = bind(&Tweener::ActRotateLocalYTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::RotateLocalZTo(Transform *target, float end, float duration) {
	Set(target, Vector3(0.f, 0.f, end), duration);
	startType = StartType::LocalRotation;
	start = target->GetLocalRotation();
	action = bind(&Tweener::ActRotateLocalZTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::RotateLocalTo(Transform *target, Vector3 end, float duration) {
	Set(target, end, duration);
	startType = StartType::LocalRotation;
	start = target->GetLocalRotation();
	action = bind(&Tweener::ActRotateLocalTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::RotateXTo(Transform *target, float end, float duration) {
	Set(target, Vector3(end, 0.f, 0.f), duration);
	startType = StartType::Rotation;
	start = target->GetRotation();
	action = bind(&Tweener::ActRotateXTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::RotateYTo(Transform *target, float end, float duration) {
	Set(target, Vector3(0.f, end, 0.f), duration);
	startType = StartType::Rotation;
	start = target->GetRotation();
	action = bind(&Tweener::ActRotateYTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::RotateZTo(Transform *target, float end, float duration) {
	Set(target, Vector3(0.f, 0.f, end), duration);
	startType = StartType::Rotation;
	start = target->GetRotation();
	action = bind(&Tweener::ActRotateZTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::RotateTo(Transform *target, Vector3 end, float duration) {
	Set(target, end, duration);
	startType = StartType::Rotation;
	start = target->GetRotation();
	action = bind(&Tweener::ActRotateTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::RotateAroundTo(Transform *target, Vector3 point, Vector3 value, float duration) {
	start = target->GetPosition();
	start2 = target->GetRotation();

	Vector3 oldPosition = target->GetPosition();
	Vector3 oldRotation = target->GetRotation();
	target->RotateAround(point, value);
	target->Update();
	Set(target, target->GetPosition(), target->GetRotation(), duration);

	target->SetPosition(oldPosition);
	target->SetRotation(oldRotation);
	target->Update();

	startType = StartType::RotationAround;
	action = bind(&Tweener::ActRotateAroundTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::ScaleLocalXTo(Transform *target, float end, float duration) {
	Set(target, Vector3(end, 0.f, 0.f), duration);
	startType = StartType::LocalScale;
	start = target->GetLocalScale();
	action = bind(&Tweener::ActScaleLocalXTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::ScaleLocalYTo(Transform *target, float end, float duration) {
	Set(target, Vector3(0.f, end, 0.f), duration);
	startType = StartType::LocalScale;
	start = target->GetLocalScale();
	action = bind(&Tweener::ActScaleLocalYTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::ScaleLocalZTo(Transform *target, float end, float duration) {
	Set(target, Vector3(0.f, 0.f, end), duration);
	startType = StartType::LocalScale;
	start = target->GetLocalScale();
	action = bind(&Tweener::ActScaleLocalZTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::ScaleLocalTo(Transform *target, Vector2 end, float duration) {
	Set(target, Vector3(end.x, end.y, 0.f), duration);
	startType = StartType::LocalScale;
	start = target->GetLocalScale();
	action = bind(&Tweener::ActScaleLocalTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::ScaleLocalTo(Transform *target, Vector3 end, float duration) {
	Set(target, end, duration);
	startType = StartType::LocalScale;
	start = target->GetLocalScale();
	action = bind(&Tweener::ActScaleLocalTo, this, placeholders::_1);

	return this;
}

Tweener *Tweener::ChangeColorTo(Drawing *target, Color end, float duration) {
	Set(target, end, duration);
	startType = StartType::Color;
	colorStart = target->GetColor();
	action = bind(&Tweener::ActChangeColorTo, this, placeholders::_1);
	return this;
}

Tweener *Tweener::FadeTo(Drawing *target, float end, float duration) {
	Set(target, Color(0.f, 0.f, 0.f, end), duration);
	startType = StartType::Color;
	colorStart = target->GetColor();
	action = bind(&Tweener::ActFadeTo, this, placeholders::_1);
	return this;
}

void Tweener::Set(Transform *target, Vector3 end, float duration) {
	this->target = target;
	this->end = end;
	this->duration = duration;
	Init();
}

void Tweener::Set(Transform *target, Vector3 end, Vector3 end2, float duration) {
	this->end2 = end2;
	Set(target, end, duration);
}

void Tweener::Set(Drawing *target, Color end, float duration) {
	colorTarget = target;
	colorEnd = end;
	this->duration = duration;
	Init();
}

void Tweener::Init() {
	timer = 0.f;
	isWaiting = false;
	isStoped = false;
	isPausing = false;
	hasRewind = false;
	isReverse = false;
	loops = 1;
	loopsNow = 1;

	callback = nullptr;
	ease = bind(&Tweener::EaseOutQuad, this, placeholders::_1, placeholders::_2, placeholders::_3);

	sync = nullptr;
}


// Init
#pragma endregion

#pragma region Update

bool Tweener::Update() {
	// Callback
	if (callback) {
		callback();
		return false;
	}

	// Don't play
	if (!isWaiting) {
		if ((target == nullptr && colorTarget == nullptr) || isStoped || isPausing || loopsNow == 0) return false;
	}

	timer += isReverse ? -Time::GetDeltaTime() : Time::GetDeltaTime();

	// Action
	if (!isWaiting) {
		float rate = (duration == 0.f) ? 1.f : Clamp(timer / duration, 0.f, 1.f);
		action(rate);
	}
	
	if (!IsPlaying()) {
		if (loopsNow > 0) {
			loopsNow--;
		} else if (loops < 0) {
			Loop();
			return true;
		}
		if (loopsNow > 0)
			Loop();
	}

	if (sync) {
		return sync->Update() || !IsEnded();
	} else {
		return IsPlaying();
	}
}

bool Tweener::IsPlaying() {
	if (isStoped || isPausing || duration == 0.f || loopsNow == 0) return false;
	if (isReverse) {
		return (timer > 0.f) ? true : false;
	} else {
		return (timer < duration) ? true : false;
	}
}

bool Tweener::IsEnded() {
	if (isStoped || duration == 0.f || loopsNow == 0) return true;
	return false;
}

// Update
#pragma endregion

#pragma region State

Tweener *Tweener::Pause() {
	isPausing = true;

	return this;
}

Tweener *Tweener::Play() {
	isPausing = false;

	return this;
}

Tweener *Tweener::Playback() {
	isPausing = false;
	isReverse = true;
	loopsNow += loops - loopsNow;

	if (sync)
		sync->Playback();

	return this;
}

Tweener *Tweener::End() {
	if (loops < 0) {
		timer = duration;
	} else {
		timer = (loops % 2 == 0) ? 0.f : duration;
	}
	loopsNow = 1;
	isReverse = false;

	if (sync)
		sync->End();

	return this;
}

Tweener *Tweener::Stop() {
	isStoped = true;

	return this;
}

Tweener *Tweener::Replay() {
	timer = 0.f;
	isPausing = false;
	isReverse = false;
	loopsNow = loops;

	if (sync)
		sync->Replay();

	return this;
}

Tweener *Tweener::SetLoop(int loops) {
	this->loops = loops;
	loopsNow = loops;

	return this;
}

Tweener *Tweener::SetRewind(bool hasRewind) {
	this->hasRewind = hasRewind;

	return this;
}

Tweener *Tweener::SetEase(Ease ease) {
	switch (ease) {
	case Ease::Linear:
		this->ease = bind(&Tweener::EaseLinear, this, placeholders::_1, placeholders::_2, placeholders::_3);
		break;
	case Ease::InSine:
		this->ease = bind(&Tweener::EaseInSine, this, placeholders::_1, placeholders::_2, placeholders::_3);
		break;
	case Ease::OutSine:
		this->ease = bind(&Tweener::EaseOutSine, this, placeholders::_1, placeholders::_2, placeholders::_3);
		break;
	case Ease::InOutSine:
		this->ease = bind(&Tweener::EaseInOutSine, this, placeholders::_1, placeholders::_2, placeholders::_3);
		break;
	case Ease::InQuad:
		this->ease = bind(&Tweener::EaseInQuad, this, placeholders::_1, placeholders::_2, placeholders::_3);
		break;
	case Ease::OutQuad:
		this->ease = bind(&Tweener::EaseOutQuad, this, placeholders::_1, placeholders::_2, placeholders::_3);
		break;
	case Ease::InOutQuad:
		this->ease = bind(&Tweener::EaseInOutQuad, this, placeholders::_1, placeholders::_2, placeholders::_3);
		break;
	}

	return this;
}

void Tweener::Loop() {
	if (hasRewind) {
		isReverse = !isReverse;
		timer = isReverse ? duration : 0.f;
	} else {
		timer = 0.f;
	}
}

// State
#pragma endregion

#pragma region Getter, Setter

float Tweener::GetDuration() {
	return duration;
}

void Tweener::ResetStart() {
	switch (startType) {
	case StartType::LocalPosition:
		start = target->GetLocalPosition();
		break;
	case StartType::Positoin:
		start = target->GetPosition();
		break;
	case StartType::LocalRotation:
		start = target->GetLocalRotation();
		break;
	case StartType::Rotation:
		start = target->GetRotation();
		break;
	case StartType::RotationAround:
		start = target->GetPosition();
		start2 = target->GetRotation();
		break;
	case StartType::LocalScale:
		start = target->GetLocalScale();
		break;
	case StartType::Color:
		colorStart = colorTarget->GetColor();
		break;
	}

	if (sync)
		sync->ResetStart();
}

void Tweener::SetSync(Tweener *sync) {
	if (this->sync) {
		this->sync->SetSync(sync);
	} else {
		this->sync = sync;
	}
}

// Getter, Setter
#pragma endregion

#pragma region Action

void Tweener::ActMoveLocalXTo(float rate) {
	target->SetLocalPositionX(ease(start.x, end.x, rate));
}

void Tweener::ActMoveLocalYTo(float rate) {
	target->SetLocalPositionY(ease(start.y, end.y, rate));
}

void Tweener::ActMoveLocalZTo(float rate) {
	target->SetLocalPositionZ(ease(start.z, end.z, rate));
}

void Tweener::ActMoveLocalTo(float rate) {
	target->SetLocalPosition(ease(start.x, end.x, rate), ease(start.y, end.y, rate), ease(start.z, end.z, rate));
}

void Tweener::ActMoveXTo(float rate) {
	target->SetPositionX(ease(start.x, end.x, rate));
}

void Tweener::ActMoveYTo(float rate) {
	target->SetPositionY(ease(start.y, end.y, rate));
}

void Tweener::ActMoveZTo(float rate) {
	target->SetPositionZ(ease(start.z, end.z, rate));
}

void Tweener::ActMoveTo(float rate) {
	target->SetPosition(ease(start.x, end.x, rate), ease(start.y, end.y, rate), ease(start.z, end.z, rate));
}

void Tweener::ActRotateLocalXTo(float rate) {
	target->SetLocalRotationX(ease(start.x, end.x, rate));
}

void Tweener::ActRotateLocalYTo(float rate) {
	target->SetLocalRotationY(ease(start.y, end.y, rate));
}

void Tweener::ActRotateLocalZTo(float rate) {
	target->SetLocalRotationZ(ease(start.z, end.z, rate));
}

void Tweener::ActRotateLocalTo(float rate) {
	target->SetLocalRotation(ease(start.x, end.x, rate), ease(start.y, end.y, rate), ease(start.z, end.z, rate));
}

void Tweener::ActRotateXTo(float rate) {
	target->SetRotationX(ease(start.x, end.x, rate));
}

void Tweener::ActRotateYTo(float rate) {
	target->SetRotationY(ease(start.y, end.y, rate));
}

void Tweener::ActRotateZTo(float rate) {
	target->SetRotationZ(ease(start.z, end.z, rate));
}

void Tweener::ActRotateTo(float rate) {
	target->SetRotation(ease(start.x, end.x, rate), ease(start.y, end.y, rate), ease(start.z, end.z, rate));
}

void Tweener::ActRotateAroundTo(float rate) {
	target->SetPosition(ease(start.x, end.x, rate), ease(start.y, end.y, rate), ease(start.z, end.z, rate));
	target->SetRotation(ease(start2.x, end2.x, rate), ease(start2.y, end2.y, rate), ease(start2.z, end2.z, rate));
}

void Tweener::ActScaleLocalXTo(float rate) {
	target->SetLocalScaleX(ease(start.x, end.x, rate));
}

void Tweener::ActScaleLocalYTo(float rate) {
	target->SetLocalScaleY(ease(start.y, end.y, rate));
}

void Tweener::ActScaleLocalZTo(float rate) {
	target->SetLocalScaleZ(ease(start.z, end.z, rate));
}

void Tweener::ActScaleLocalTo(float rate) {
	target->SetLocalScale(ease(start.x, end.x, rate), ease(start.y, end.y, rate), ease(start.z, end.z, rate));
}

void Tweener::ActChangeColorTo(float rate) {
	colorTarget->SetColor(ease(colorStart.r, colorEnd.r, rate), ease(colorStart.g, colorEnd.g, rate), ease(colorStart.b, colorEnd.b, rate), ease(colorStart.a, colorEnd.a, rate));
}

void Tweener::ActFadeTo(float rate) {
	colorTarget->SetAlpha(ease(colorStart.a, colorEnd.a, rate));
}

// Action
#pragma endregion

#pragma region Easing

float Tweener::EaseLinear(float start, float end, float rate) {
	return start + (end - start) * rate;
}

float Tweener::EaseInSine(float start, float end, float rate) {
	return -(end -= start) * cos(rate * HALF_PI) + end + start;
}

float Tweener::EaseOutSine(float start, float end, float rate) {
	return  (end - start) * sin(rate * HALF_PI) + start;
}

float Tweener::EaseInOutSine(float start, float end, float rate) {
	return -(end -= start) * 0.5f * (cos(D3DX_PI * rate) - 1.f) + start;
}

float Tweener::EaseInQuad(float start, float end, float rate) {
	return (end - start) * rate * rate + start;
}

float Tweener::EaseOutQuad(float start, float end, float rate) {
	return -(end - start) * rate * (rate - 2.f) + start;
}

float Tweener::EaseInOutQuad(float start, float end, float rate) {
	if ((rate *= 2.f) < 1.f) {
		printf("%f", (end - start) * 0.5f * rate * rate + start);
		return (end - start) * 0.5f * rate * rate + start;
	} else {
		return -(end - start) * 0.5f * (--rate * (rate - 2.f) - 1.f) + start;
	}
}

// Easing
#pragma endregion