// Tweener.h
// 補間アニメーション
//
// Transform animation that set with end and duration, and can set easing.
// 終わりと時間で決めて、緩急も付けれるのトランスフォームアニメーション。

#ifndef __TWEENER_H__
#define __TWEENER_H__

#include <functional>
#include "..\Draw\Drawing.h"

using namespace std;

enum class Ease {
	Linear,
	InSine,
	OutSine,
	InOutSine,
	InQuad,
	OutQuad,
	InOutQuad
	//InCubic,
	//OutCubic,
	//InOutCubic,
	//InQuart,
	//OutQuart,
	//InOutQuart,
	//InQuint,
	//OutQuint,
	//InOutQuint,
	//InExpo,
	//OutExpo,
	//InOutExpo,
	//InCirc,
	//OutCirc,
	//InOutCirc,
	//InBack,
	//OutBack,
	//InOutBack,
	//InElastic,
	//OutElastic,
	//InOutElastic,
	//InBounce,
	//OutBounce,
	//InOutBounce
};

class Tweener {
public:
	bool inSequence = false;

	// Init

	Tweener *Wait(float duration);
	Tweener *SetCallback(function<void()> callback);

	Tweener *MoveLocalXTo(Transform *target, float end, float duration);
	Tweener *MoveLocalYTo(Transform *target, float end, float duration);
	Tweener *MoveLocalZTo(Transform *target, float end, float duration);
	Tweener *MoveLocalTo(Transform *target, Vector2 end, float duration);
	Tweener *MoveLocalTo(Transform *target, Vector3 end, float duration);
	Tweener *MoveXTo(Transform *target, float end, float duration);
	Tweener *MoveYTo(Transform *target, float end, float duration);
	Tweener *MoveZTo(Transform *target, float end, float duration);
	Tweener *MoveTo(Transform *target, Vector2 end, float duration);
	Tweener *MoveTo(Transform *target, Vector3 end, float duration);
	
	Tweener *RotateLocalXTo(Transform *target, float end, float duration);
	Tweener *RotateLocalYTo(Transform *target, float end, float duration);
	Tweener *RotateLocalZTo(Transform *target, float end, float duration);
	Tweener *RotateLocalTo(Transform *target, Vector3 end, float duration);
	Tweener *RotateXTo(Transform *target, float end, float duration);
	Tweener *RotateYTo(Transform *target, float end, float duration);
	Tweener *RotateZTo(Transform *target, float end, float duration);
	Tweener *RotateTo(Transform *target, Vector3 end, float duration);
	Tweener *RotateAroundTo(Transform *target, Vector3 point, Vector3 value, float duration);

	Tweener *ScaleLocalXTo(Transform *target, float end, float duration);
	Tweener *ScaleLocalYTo(Transform *target, float end, float duration);
	Tweener *ScaleLocalZTo(Transform *target, float end, float duration);
	Tweener *ScaleLocalTo(Transform *target, Vector2 end, float duration);
	Tweener *ScaleLocalTo(Transform *target, Vector3 end, float duration);

	Tweener *ChangeColorTo(Drawing *target, Color end, float duration);
	Tweener *FadeTo(Drawing *target, float end, float duration);

	// Update

	bool Update();
	bool IsPlaying();
	bool IsEnded();

	// State

	Tweener *Pause();
	Tweener *Play();
	Tweener *Playback();
	Tweener *End();
	Tweener *Stop();
	Tweener *Replay();
	Tweener *SetLoop(int loops);
	Tweener *SetRewind(bool hasRewind);
	Tweener *SetEase(Ease ease);

	// Getter, Setter

	float GetDuration();
	void ResetStart();
	void SetSync(Tweener *sync);

private:
	Transform *target;
	Drawing *colorTarget;

	enum class StartType {
		None,

		Positoin,
		LocalPosition,
		Rotation,
		RotationAround,
		LocalRotation,
		LocalScale,
		Color
	};
	StartType startType;

	Vector3 start;
	Vector3 end;
	Vector3 start2;
	Vector3 end2;
	Color colorStart;
	Color colorEnd;
	float duration = 0.f;
	float timer = 0.f;
	
	bool isWaiting = false;
	bool isStoped = false;
	bool isPausing = false;
	bool hasRewind = false;
	bool isReverse = false;
	int loops = 1;
	int loopsNow = 1;

	Tweener *sync = nullptr;

	function<void()> callback;
	function<void(float)> action;
	function<float(float, float, float)> ease = bind(&Tweener::EaseOutQuad, this, placeholders::_1, placeholders::_2, placeholders::_3);

	// Init

	void Set(Transform *target, Vector3 end, float duration);
	void Set(Transform *target, Vector3 end, Vector3 end2, float duration);
	void Set(Drawing *target, Color end, float duration);
	void Init();

	// State

	void Loop();

	// Action

	void ActMoveLocalXTo(float rate);
	void ActMoveLocalYTo(float rate);
	void ActMoveLocalZTo(float rate);
	void ActMoveLocalTo(float rate);
	void ActMoveXTo(float rate);
	void ActMoveYTo(float rate);
	void ActMoveZTo(float rate);
	void ActMoveTo(float rate);

	void ActRotateLocalXTo(float rate);
	void ActRotateLocalYTo(float rate);
	void ActRotateLocalZTo(float rate);
	void ActRotateLocalTo(float rate);
	void ActRotateXTo(float rate);
	void ActRotateYTo(float rate);
	void ActRotateZTo(float rate);
	void ActRotateTo(float rate);
	void ActRotateAroundTo(float rate);

	void ActScaleLocalXTo(float rate);
	void ActScaleLocalYTo(float rate);
	void ActScaleLocalZTo(float rate);
	void ActScaleLocalTo(float rate);

	void ActChangeColorTo(float rate);
	void ActFadeTo(float rate);

	// Easing

	float EaseLinear(float start, float end, float rate);
	float EaseInSine(float start, float end, float rate);
	float EaseOutSine(float start, float end, float rate);
	float EaseInOutSine(float start, float end, float rate);
	float EaseInQuad(float start, float end, float rate);
	float EaseOutQuad(float start, float end, float rate);
	float EaseInOutQuad(float start, float end, float rate);
};

#endif