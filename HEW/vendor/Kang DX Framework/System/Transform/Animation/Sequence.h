// Sequence.h
// 補間アニメーション序列
//
// Group of the tweeners will play contiuned.
// 連続でプレイする補間アニメーション群。

#ifndef __SEQUENCE_H__
#define __SEQUENCE_H__

#include <functional>
#include "Tweener.h"

class Sequence {
public:
	// Init & Fin & Update

	Sequence();
	~Sequence();

	void Init();
	void AddInEnd(Tweener* tweener);
	void AddWithLast(Tweener* tweener);
	void Clear();
	list<Tweener*> *GetTweeners();

	bool Update();
	bool IsPlaying();
	bool IsEnded();

	// State

	Sequence *Pause();
	Sequence *Play();
	Sequence *End();
	Sequence *Stop();
	// loops = -1: Infinite loop.
	Sequence *SetLoop(int loops);
	Sequence *SetRewind(bool hasRewind);

private:
	list<Tweener*> *tweeners;
	list<Tweener*> *syncTweeners;
	list<Tweener*>::iterator nowTweener;

	bool isStoped = false;
	bool isPausing = false;
	bool hasRewind = false;
	bool isReverse = false;
	bool isReverseEnded = false;
	int loops = 1;
	int loopsNow = 1;

	void Loop();
};

#endif