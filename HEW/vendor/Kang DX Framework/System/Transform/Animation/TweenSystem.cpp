// TweenSystem.cpp
// 補間アニメーションシステム
//
// System of transform animation's objects mangemaent. Call the functions by class name.
// 補間アニメーションのオブジェクトを管理するシステム。クラス名から関数を呼び出す。

#include "TweenSystem.h"

ObjectPool<Tweener> *TweenSystem::tweeners = new ObjectPool<Tweener>(0);
list<Tweener*> *TweenSystem::playingTweeners = new list<Tweener*>();
ObjectPool<Sequence> *TweenSystem::sequences = new ObjectPool<Sequence>(0);
list<Sequence*> *TweenSystem::playingSequences = new list<Sequence*>();

#pragma region Action

Tweener *TweenSystem::Wait(float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->Wait(duration);
}

Tweener *TweenSystem::SetCallback(function<void()> callback) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->SetCallback(callback);
}

Tweener *TweenSystem::MoveLocalXTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->MoveLocalXTo(target, end, duration);
}

Tweener *TweenSystem::MoveLocalYTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->MoveLocalYTo(target, end, duration);
}

Tweener *TweenSystem::MoveLocalZTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->MoveLocalZTo(target, end, duration);
}

Tweener *TweenSystem::MoveLocalTo(Transform *target, Vector2 end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->MoveLocalTo(target, end, duration);
}

Tweener *TweenSystem::MoveLocalTo(Transform *target, Vector3 end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->MoveLocalTo(target, end, duration);
}

Tweener *TweenSystem::MoveXTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->MoveXTo(target, end, duration);
}

Tweener *TweenSystem::MoveYTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->MoveYTo(target, end, duration);
}

Tweener *TweenSystem::MoveZTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->MoveZTo(target, end, duration);
}

Tweener *TweenSystem::MoveTo(Transform *target, Vector2 end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->MoveTo(target, end, duration);
}

Tweener *TweenSystem::MoveTo(Transform *target, Vector3 end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->MoveTo(target, end, duration);
}

Tweener *TweenSystem::RotateLocalXTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->RotateLocalXTo(target, end, duration);
}

Tweener *TweenSystem::RotateLocalYTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->RotateLocalYTo(target, end, duration);
}

Tweener *TweenSystem::RotateLocalZTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->RotateLocalZTo(target, end, duration);
}

Tweener *TweenSystem::RotateLocalTo(Transform *target, Vector3 end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->RotateLocalTo(target, end, duration);
}

Tweener *TweenSystem::RotateXTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->RotateXTo(target, end, duration);
}

Tweener *TweenSystem::RotateYTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->RotateYTo(target, end, duration);
}

Tweener *TweenSystem::RotateZTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->RotateZTo(target, end, duration);
}

Tweener *TweenSystem::RotateTo(Transform *target, Vector3 end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->RotateTo(target, end, duration);
}

Tweener *TweenSystem::RotateAroundTo(Transform *target, Vector3 point, Vector3 value, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->RotateAroundTo(target, point, value, duration);
}

Tweener *TweenSystem::ScaleLocalXTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->ScaleLocalXTo(target, end, duration);
}

Tweener *TweenSystem::ScaleLocalYTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->ScaleLocalYTo(target, end, duration);
}

Tweener *TweenSystem::ScaleLocalZTo(Transform *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->ScaleLocalZTo(target, end, duration);
}

Tweener *TweenSystem::ScaleLocalTo(Transform *target, Vector2 end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->ScaleLocalTo(target, end, duration);
}

Tweener *TweenSystem::ScaleLocalTo(Transform *target, Vector3 end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->ScaleLocalTo(target, end, duration);
}

Tweener *TweenSystem::ChangeColorTo(Drawing *target, Color end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->ChangeColorTo(target, end, duration);
}

Tweener *TweenSystem::FadeTo(Drawing *target, float end, float duration) {
	playingTweeners->push_back(tweeners->Get());
	return playingTweeners->back()->FadeTo(target, end, duration);
}

Sequence *TweenSystem::GetSequence() {
	playingSequences->push_back(sequences->Get());
	playingSequences->back()->Init();
	return playingSequences->back();
}

// Action
#pragma endregion

#pragma region Update

void TweenSystem::Update() {
	// Check tweeners.
	for (auto tweener = playingTweeners->begin(); tweener != playingTweeners->end();) {
		if ((*tweener)->inSequence) {
			(*tweener)->inSequence = false;
			playingTweeners->erase(tweener++);
		} else if (!(*tweener)->Update() && (*tweener)->IsEnded()) {
			tweeners->Putback(*tweener);
			playingTweeners->erase(tweener++);
		} else {
			tweener++;
		}
	}

	// Check sequences.
	for (auto sequence = playingSequences->begin(); sequence != playingSequences->end();) {
		if (!(*sequence)->Update() && (*sequence)->IsEnded()) {
 			for (auto &tweener : *(*sequence)->GetTweeners())
				tweeners->Putback(tweener);
			(*sequence)->Clear();
			sequences->Putback(*sequence);
			playingSequences->erase(sequence++);
		} else {
			sequence++;
		}
	}
}

// Update
#pragma endregion