// TweenSystem.h
// 補間アニメーションシステム
//
// System of transform animation's objects mangemaent. Call the functions by class name.
// 補間アニメーションのオブジェクトを管理するシステム。クラス名から関数を呼び出す。

#ifndef __TWEEN_SYSTEM_H__
#define __TWEEN_SYSTEM_H__

#include "..\..\ObjectPool.h"
#include "Tweener.h"
#include "Sequence.h"

class TweenSystem {
public:
	// Action

	static Tweener *Wait(float duration);
	static Tweener *SetCallback(function<void()> callback);

	static Tweener *MoveLocalXTo(Transform *target, float end, float duration);
	static Tweener *MoveLocalYTo(Transform *target, float end, float duration);
	static Tweener *MoveLocalZTo(Transform *target, float end, float duration);
	static Tweener *MoveLocalTo(Transform *target, Vector2 end, float duration);
	static Tweener *MoveLocalTo(Transform *target, Vector3 end, float duration);
	static Tweener *MoveXTo(Transform *target, float end, float duration);
	static Tweener *MoveYTo(Transform *target, float end, float duration);
	static Tweener *MoveZTo(Transform *target, float end, float duration);
	static Tweener *MoveTo(Transform *target, Vector2 end, float duration);
	static Tweener *MoveTo(Transform *target, Vector3 end, float duration);

	static Tweener *RotateLocalXTo(Transform *target, float end, float duration);
	static Tweener *RotateLocalYTo(Transform *target, float end, float duration);
	static Tweener *RotateLocalZTo(Transform *target, float end, float duration);
	static Tweener *RotateLocalTo(Transform *target, Vector3 end, float duration);
	static Tweener *RotateXTo(Transform *target, float end, float duration);
	static Tweener *RotateYTo(Transform *target, float end, float duration);
	static Tweener *RotateZTo(Transform *target, float end, float duration);
	static Tweener *RotateTo(Transform *target, Vector3 end, float duration);
	static Tweener *RotateAroundTo(Transform *target, Vector3 point, Vector3 value, float duration);

	static Tweener *ScaleLocalXTo(Transform *target, float end, float duration);
	static Tweener *ScaleLocalYTo(Transform *target, float end, float duration);
	static Tweener *ScaleLocalZTo(Transform *target, float end, float duration);
	static Tweener *ScaleLocalTo(Transform *target, Vector2 end, float duration);
	static Tweener *ScaleLocalTo(Transform *target, Vector3 end, float duration);
	static Tweener *ChangeColorTo(Drawing *target, Color end, float duration);

	static Tweener *FadeTo(Drawing *target, float end, float duration);

	static Sequence *GetSequence();

	// Update

	static void Update();

private:
	static ObjectPool<Tweener> *tweeners;
	static list<Tweener*> *playingTweeners;
	static ObjectPool<Sequence> *sequences;
	static list<Sequence*> *playingSequences;
};

#endif