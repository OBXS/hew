#ifndef __COLLIDER_TYPE_H__
#define __COLLIDER_TYPE_H__

enum class ColliderType {
	Default,
	UI,
	Input,
	Player,

	Num
};

enum class Collider2DShape {
	None = -1,

	Rect,
	Circle,

	Num
};

#endif