#ifndef __COLLISION_2D_SYSTEM_H__
#define __COLLISION_2D_SYSTEM_H__

#include "..\..\ObjectPool.h"
#include "..\Draw.h"
#include "Raycast.h"

class Collision2DSystem {
public:
	// Recommend to use in initialization.
	static void DisableChackTableExceptDefault();
	// isBoth = false: only set type to all type.
	static void SetChackTableOneForAll(ColliderType type, bool isActive, bool isBoth = true);
	// isBoth = false: only set type1 to type2.
	static void SetChackTable(ColliderType type1, ColliderType type2, bool isActive, bool isBoth = true);
	static void Update();
	static void DrawShapes();

	// Get a new collider.
	static Collider2D* PickOut(ColliderType type);
	static void PutBack(Collider2D* col);
	static void DisableAll();
	static void ClearAll();

	static list<Collider2D*> *CheckPoint(Vector2 point, ColliderType type);
	static RaycastResult Raycast(Vector2 start, Vector2 end, ColliderType type);
	static RaycastResult Raycast(Vector2 start, Vector2 dir, float dist, ColliderType type);
	static RaycastResult Raycast(Ray ray, ColliderType type);
	static bool Overlap(Collider2D *col1, Collider2D* col2);
	static bool OverlapPoint(Vector2 point, Collider2D *col);

private:
	static ObjectPool<Collider2D> *colliderPool;
	static list<Collider2D*> *colliders;
	static list<Collider2D*> *disabledColliders;
	static bool *checkTable;

	static Rect *rect;
	static Ellip *circle;

	static void CheckShape(list<Collider2D*> *cols);
	static void DrawShape(Collider2D *col, Shape *shape);

	static void CheckDisabledColliders();
	static void CheckOrder(Collider2D *collider);

	static bool OverlapCircles(Collider2D *col1, Collider2D *col2);
	static bool CircleOverlapRect(Collider2D *circleCol, Collider2D *rectCol);
	static bool OverlapRects(Collider2D *col1, Collider2D *col2);
	static bool PointInRect(Vector2 point, Vector2 *rect);
	static bool CrossLine(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4);
	static RaycastResult RaycastCircle(Ray ray, Collider2D *col, RaycastResult result);
	static RaycastResult RaycastRect(Ray ray, Collider2D *col, RaycastResult result);
	static RaycastResult RaycastRect(Ray ray, Ray seg, RaycastResult result);
};

#endif