// Raycast.h
// レイキャスト
//
// Structure for raycasting.
// レイキャスト用の構造体。

#ifndef __RAYCAST_H__
#define __RAYCAST_H__

#include "..\..\Global.h"
#include "Collider2D.h"

struct Ray {
	Vector2 start;
	Vector2 end;
	float length;

	Ray() {}

	Ray(Vector2 start, Vector2 end) {
		this->start = start;
		this->end = end;
		length = D3DXVec2Length(&(start - end));
	}

	Ray(Vector2 start, Vector2 dir, float lenght) {
		this->start = start;
		D3DXVec2Normalize(&dir, &dir);
		end = start + dir * lenght;
		this->length = length;
	}
};

struct RaycastResult {
	bool isCollided = false;
	bool isUpdated = false;
	Collider2D *target = nullptr;
	Vector2 collidedPoint;
};

#endif