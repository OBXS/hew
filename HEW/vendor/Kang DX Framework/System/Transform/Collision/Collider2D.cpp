#include "Collider2D.h"

void Collider2D::Init() {
	Init(Collider2DShape::None, Vector2(100.f, 100.f), 0.f, nullptr);
}

Collider2D *Collider2D::Init(Collider2DShape shape, Transform *parent) {
	return Init(shape, Vector2(100.f, 100.f), 0.f, parent);
}

Collider2D *Collider2D::Init(Collider2DShape shape, Vector2 size, Transform *parent) {
	return Init(shape, size, 0.f, parent);
}

Collider2D *Collider2D::Init(Collider2DShape shape, Vector2 size, float angle, Transform *parent) {
	this->shape = shape;
	this->type = type;
	this->size = size;
	this->angle = angle;
	this->SetParent(parent);

	return this;
}

void Collider2D::SetActive(bool isActive) {
	isEnabled = isActive;
	isDebugShowed = isActive;
}

Collider2DShape Collider2D::GetShape() {
	return shape;
}

Vector2 Collider2D::GetSize() {
	float x = size.x * GetScale().x;
	float y = size.y * GetScale().y;
	if (shape == Collider2DShape::Circle)
		x = y = fminf(x, y);
	return Vector2(x, y);
}

Vector2 Collider2D::GetHalfSize() {
	return GetSize() * 0.5f;
}

float Collider2D::GetRange() {
	return ((shape == Collider2DShape::Circle) ? fminf(GetHalfSize().x, GetHalfSize().y) : D3DXVec2Length(&GetHalfSize()));
}

float Collider2D::GetAngle() {
	return angle + GetRotation().z;
}