#ifndef __COLLIDER_2D_H__
#define __COLLIDER_2D_H__

#include<functional>
#include "..\Transform.h"
#include "ColliderType.h"

class Collider2D : public Transform {
public:
	bool isEnabled = true;
	bool isDebugShowed = true;
	int hit = false;
	ColliderType type = ColliderType::Default;

	// Will be called when collided.
	std::function<void(Collider2D*, Collider2D*)> hitHandler;

	void Init();
	Collider2D *Init(Collider2DShape shape, Transform *parent = nullptr);
	Collider2D *Init(Collider2DShape shape, Vector2 size, Transform *parent = nullptr);
	Collider2D *Init(Collider2DShape shape, Vector2 size, float angle, Transform *parent = nullptr);

	void SetActive(bool isActive);
	Collider2DShape GetShape();
	Vector2 GetSize();
	Vector2 GetHalfSize();
	float GetRange();
	float GetAngle();

private:
	Collider2DShape shape = Collider2DShape::None;
	Vector2 size = Vector2(100.f, 100.f);
	float angle = 0.f;
};

#endif