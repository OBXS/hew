﻿#include "Collision2DSystem.h"

ObjectPool<Collider2D> *Collision2DSystem::colliderPool = new ObjectPool<Collider2D>(0);
list<Collider2D*> *Collision2DSystem::colliders = new list<Collider2D*>[(int)ColliderType::Num];
list<Collider2D*> *Collision2DSystem::disabledColliders = new list<Collider2D*>();
bool *Collision2DSystem::checkTable = new bool[(int)ColliderType::Num * (int)ColliderType::Num];

Rect *Collision2DSystem::rect = new Rect(Vector2(1.f, 1.f));
Ellip *Collision2DSystem::circle = new Ellip(1000.f);

#pragma region Colliders

void Collision2DSystem::DisableChackTableExceptDefault() {
	for (int i = 0; i < (int)ColliderType::Num * (int)ColliderType::Num; i++)
		checkTable[i] = (i < (int)ColliderType::Num || i % (int)ColliderType::Num == 0) ? true : false;
}

void Collision2DSystem::SetChackTableOneForAll(ColliderType type, bool isActive, bool isBoth) {
	for (int i = (int)type * (int)ColliderType::Num; i < ((int)type + 1) * (int)ColliderType::Num; i++) {
		checkTable[i] = isActive;
		if (isBoth) {
			int j = i - (int)type * (int)ColliderType::Num;
			checkTable[j * (int)ColliderType::Num + (int)type] = isActive;
		}
	}
}

void Collision2DSystem::SetChackTable(ColliderType type1, ColliderType type2, bool isActive, bool isBoth) {
	checkTable[(int)type1 * (int)ColliderType::Num + (int)type2] = isActive;
	if (isBoth)
		checkTable[(int)type2 * (int)ColliderType::Num + (int)type1] = isActive;
}

void Collision2DSystem::Update() {
	CheckDisabledColliders();

	for (int i = 0; i < (int)ColliderType::Num; i++) {
		for (auto col = colliders[i].begin(); col != colliders[i].end();) {
			if (!(*col)->isEnabled) {
				disabledColliders->push_back(*col);
				colliders[i].erase(col++);
			} else {
				(*col)->Update();
				CheckOrder(*col);
				col++;
			}
		}
	}
}


void Collision2DSystem::DrawShapes() {
	for (int i = 0; i < (int)ColliderType::Num; i++)
		CheckShape(&colliders[i]);
	CheckShape(disabledColliders);
}

void Collision2DSystem::CheckShape(list<Collider2D*> *cols) {
	for (auto &col : *cols) {
		if (!col->isDebugShowed) continue;

		switch (col->GetShape()) {
		case Collider2DShape::Rect:
			rect->SetLocalScale(col->GetSize());
			DrawShape(col, rect);
			break;
		case Collider2DShape::Circle:
			circle->SetLocalScale(col->GetSize() * 0.001f);
			DrawShape(col, circle);
			break;
		}
	}
}

void Collision2DSystem::DrawShape(Collider2D *col, Shape *shape) {
	shape->SetLocalPosition(col->GetPosition2());
	shape->SetLocalPositionZ(-1500.f);
	shape->SetLocalRotationZ(col->GetAngle());
	if (!col->isEnabled) {
		shape->SetColor(Color(0.f, 0.2f, 0.f, 0.5f));
	} else if (col->hit) {
		shape->SetColor(Color(0.f, 1.f, 0.f, 0.5f));
	} else {
		shape->SetColor(Color(0.f, 0.6f, 0.f, 0.5f));
	}
	shape->Draw();
	col->hit = false;
}

Collider2D* Collision2DSystem::PickOut(ColliderType type) {
	Collider2D* col = colliderPool->Get();
	col->type = type;
	col->Init();
	colliders[(int)type].push_back(col);

	return col;
}

void Collision2DSystem::PutBack(Collider2D* col) {
	disabledColliders->remove(col);
	colliders[(int)col->type].remove(col);
	colliderPool->Putback(col);
}

void Collision2DSystem::DisableAll() {
	for (int i = 0; i < (int)ColliderType::Num; i++) {
		for (auto &col : colliders[i])
			disabledColliders[i].push_back(col);
		colliders[i].clear();
	}
}

void Collision2DSystem::ClearAll() {
	for (int i = 0; i < (int)ColliderType::Num; i++) {
		for (auto collider = disabledColliders[i].begin(); collider != disabledColliders[i].end();) {
			disabledColliders->remove(*collider);
			disabledColliders[i].erase(collider++);
		}

		for (auto collider = colliders[i].begin(); collider != colliders[i].end();) {
			colliders->remove(*collider);
			colliders[i].erase(collider++);
		}
	}
}

void Collision2DSystem::CheckDisabledColliders() {
	for (auto collider = disabledColliders->begin(); collider != disabledColliders->end();) {
		if ((*collider)->isEnabled) {
			colliders->push_back(*collider);
			disabledColliders->erase(collider++);
		} else {
			collider++;
		}
	}
}

// Colliders
#pragma endregion

#pragma region Check

void Collision2DSystem::CheckOrder(Collider2D *col) {
	for (int i = (int)col->type; i < (int)ColliderType::Num; i++) {
		if (checkTable[(int)col->type * (int)ColliderType::Num + i]) {
			for (auto &other : colliders[i]) {
				if (Overlap(col, other)) {
					if (col->hitHandler)
						col->hitHandler(col, other);
					col->hit = true;
					other->hit = true;
				}
			}
		}
	}
}

bool Collision2DSystem::Overlap(Collider2D *col1, Collider2D* col2) {
	if (col1 == col2) return false;
	if (!col1->isEnabled || !col2->isEnabled) return false;

	switch (col1->GetShape()) {
	case Collider2DShape::Rect:
		if (col2->GetShape() == Collider2DShape::Rect) {
			return OverlapRects(col1, col2);
		} else if (col2->GetShape() == Collider2DShape::Circle) {
			return CircleOverlapRect(col2, col1);
		} else {
			return false;
		}

	case Collider2DShape::Circle:
		if (col2->GetShape() == Collider2DShape::Rect) {
			return CircleOverlapRect(col1, col2);
		} else if (col2->GetShape() == Collider2DShape::Circle) {
			return OverlapCircles(col1, col2);
		} else  {
			return false;
		}

	default:
		return false;
	}
}

list<Collider2D*> *Collision2DSystem::CheckPoint(Vector2 point, ColliderType type) {
	list<Collider2D*> *cols = new list<Collider2D*>();
	for (int i = 0; i < (int)ColliderType::Num; i++) {
		if (checkTable[(int)type * (int)ColliderType::Num + i]) {
			for (auto &other : colliders[i]) {
				if (OverlapPoint(point, other))
					cols->push_back(other);
			}
		}
	}
	return (cols->size() > 0) ? cols : nullptr;
}

RaycastResult Collision2DSystem::Raycast(Vector2 start, Vector2 end, ColliderType type) {
	return Raycast(Ray(start, end), type);
}

RaycastResult Collision2DSystem::Raycast(Vector2 start, Vector2 dir, float lenght, ColliderType type) {
	return Raycast(Ray(start, dir, lenght), type);
}

RaycastResult Collision2DSystem::Raycast(Ray ray, ColliderType type) {
	RaycastResult result;

	for (int i = 0; i < (int)ColliderType::Num; i++) {
		if (checkTable[(int)type * (int)ColliderType::Num + i]) {
			for (auto &other : colliders[i]) {
				switch (other->GetShape()) {
				case Collider2DShape::Rect:
					result = RaycastRect(ray, other, result);
					break;
				case Collider2DShape::Circle:
					result = RaycastCircle(ray, other, result);
					break;
				}
			}
		}
	}

	return result;
}

// Check
#pragma endregion

#pragma region Overlap

bool Collision2DSystem::OverlapPoint(Vector2 point, Collider2D *col) {
	if (!col->isEnabled) return false;

	switch (col->GetShape()) {
	case Collider2DShape::Rect:
		if (point.x >= col->GetPosition().x - col->GetHalfSize().x && point.x <= col->GetPosition().x + col->GetHalfSize().x
			&& point.y >= col->GetPosition().y - col->GetHalfSize().y && point.y <= col->GetPosition().y + col->GetHalfSize().y) {
			return true;
		} else {
			return false;
		}
	case Collider2DShape::Circle:
		if (D3DXVec2LengthSq(&(point - col->GetPosition2())) <= col->GetRange() * col->GetRange()) {
			return true;
		} else {
			return false;
		}
	default:
		return false;
	}
}

bool Collision2DSystem::OverlapCircles(Collider2D *col1, Collider2D *col2) {
	float distance = D3DXVec2LengthSq(&(col1->GetPosition2() - col2->GetPosition2()));
	float totalRange = (col1->GetRange() + col2->GetRange()) * (col1->GetRange() + col2->GetRange());
	return (distance <= totalRange) ? true : false;
}

bool Collision2DSystem::CircleOverlapRect(Collider2D *circleCol, Collider2D *rectCol) {
	// 最大距離チェック
	if (D3DXVec2Length(&(circleCol->GetPosition2() - rectCol->GetPosition2())) > circleCol->GetRange() + rectCol->GetRange()) return false;

	// 逆回転
	float rad = -D3DXToRadian(rectCol->GetAngle());
	float rCos = cosf(rad);
	float rSin = sinf(rad);

	// 矩形からの距離を取る
	Vector2 pos = circleCol->GetPosition2() - rectCol->GetPosition2();
	// 円の位置を回転前に戻る
	float x = pos.x * rCos - pos.y * rSin;
	float y = pos.x * rSin + pos.y * rCos;
	pos.x = x;
	pos.y = y;
	// 矩形の中で最も円に近い点を算出
	float halfWidth = rectCol->GetHalfSize().x;
	float halfHeight = rectCol->GetHalfSize().y;

	Vector2 checkPoint;
	if (pos.x < -halfWidth)
		checkPoint.x = -halfWidth;
	else if (pos.x > halfWidth)
		checkPoint.x = halfWidth;
	else
		checkPoint.x = pos.x;

	if (pos.y < -halfHeight)
		checkPoint.y = -halfHeight;
	else if (pos.y > halfHeight)
		checkPoint.y = halfHeight;
	else
		checkPoint.y = pos.y;

	// 接触判定
 	return D3DXVec2Length(&(pos - checkPoint)) <= circleCol->GetRange();
}

bool Collision2DSystem::OverlapRects(Collider2D *col1, Collider2D *col2) {
	Vector2 cpos = col1->GetPosition2();
	Vector2 tpos = col2->GetPosition2();

	//絶対に接触しない距離なら判定回避
	float dist = D3DXVec2Length(&(cpos - tpos));
	if (dist > col1->GetRange() + col2->GetRange()) return false;

	float rad = D3DXToRadian(col1->GetAngle());
	float cos1 = cosf(rad);
	float sin1 = sinf(rad);
	rad = D3DXToRadian(col2->GetAngle());
	float cos2 = cosf(rad);
	float sin2 = sinf(rad);
	float cRangeX = col1->GetHalfSize().x;
	float cRangeY = col1->GetHalfSize().y;
	float tRangeX = col2->GetHalfSize().x;
	float tRangeY = col2->GetHalfSize().y;

	Vector2* pt1 = new Vector2[4];
	Vector2* pt2 = new Vector2[4];
	//自分の4頂点を取得
	pt1[0].x = -cRangeX * cos1 -  cRangeY * sin1 + cpos.x;
	pt1[0].y = -cRangeX * sin1 +  cRangeY * cos1 + cpos.y;
	pt1[1].x =  cRangeX * cos1 -  cRangeY * sin1 + cpos.x;
	pt1[1].y =  cRangeX * sin1 +  cRangeY * cos1 + cpos.y;
	pt1[2].x =  cRangeX * cos1 - -cRangeY * sin1 + cpos.x;
	pt1[2].y =  cRangeX * sin1 + -cRangeY * cos1 + cpos.y;
	pt1[3].x = -cRangeX * cos1 - -cRangeY * sin1 + cpos.x;
	pt1[3].y = -cRangeX * sin1 + -cRangeY * cos1 + cpos.y;
	//対象の4頂点を取得
	pt2[0].x = -tRangeX * cos2 -  tRangeY * sin2 + tpos.x;
	pt2[0].y = -tRangeX * sin2 +  tRangeY * cos2 + tpos.y;
	pt2[1].x =  tRangeX * cos2 -  tRangeY * sin2 + tpos.x;
	pt2[1].y =  tRangeX * sin2 +  tRangeY * cos2 + tpos.y;
	pt2[2].x =  tRangeX * cos2 - -tRangeY * sin2 + tpos.x;
	pt2[2].y =  tRangeX * sin2 + -tRangeY * cos2 + tpos.y;
	pt2[3].x = -tRangeX * cos2 - -tRangeY * sin2 + tpos.x;
	pt2[3].y = -tRangeX * sin2 + -tRangeY * cos2 + tpos.y;

	//上辺
	if (CrossLine(pt1[0], pt1[1], pt2[0], pt2[1]))
		return true;
	if (CrossLine(pt1[0], pt1[1], pt2[1], pt2[2]))
		return true;
	if (CrossLine(pt1[0], pt1[1], pt2[2], pt2[3]))
		return true;
	if (CrossLine(pt1[0], pt1[1], pt2[3], pt2[0]))
		return true;
	//右辺
	if (CrossLine(pt1[1], pt1[2], pt2[0], pt2[1]))
		return true;
	if (CrossLine(pt1[1], pt1[2], pt2[1], pt2[2]))
		return true;
	if (CrossLine(pt1[1], pt1[2], pt2[2], pt2[3]))
		return true;
	if (CrossLine(pt1[1], pt1[2], pt2[3], pt2[0]))
		return true;
	//下辺
	if (CrossLine(pt1[2], pt1[3], pt2[0], pt2[1]))
		return true;
	if (CrossLine(pt1[2], pt1[3], pt2[1], pt2[2]))
		return true;
	if (CrossLine(pt1[2], pt1[3], pt2[2], pt2[3]))
		return true;
	if (CrossLine(pt1[2], pt1[3], pt2[3], pt2[0]))
		return true;
	//左辺
	if (CrossLine(pt1[3], pt1[0], pt2[0], pt2[1]))
		return true;
	if (CrossLine(pt1[3], pt1[0], pt2[1], pt2[2]))
		return true;
	if (CrossLine(pt1[3], pt1[0], pt2[2], pt2[3]))
		return true;
	if (CrossLine(pt1[3], pt1[0], pt2[3], pt2[0]))
		return true;

	//対象が自分に内包されているか
	pt1[0] = Vector2(-cRangeX,  cRangeY);
	pt1[1] = Vector2( cRangeX,  cRangeY);
	pt1[2] = Vector2( cRangeX, -cRangeY);
	pt1[3] = Vector2(-cRangeX, -cRangeY);
	Vector2 point = tpos - cpos;
	Vector2 checkPt;
	checkPt.x = point.x *  cos1 - point.y * -sin1;//逆回転はsinが反転
	checkPt.y = point.x * -sin1 + point.y *  cos1;
	if (PointInRect(checkPt, pt1))
		return true;

	//自分が対象に内包されているか
	pt2[0] = Vector2(-tRangeX,  tRangeY);
	pt2[1] = Vector2( tRangeX,  tRangeY);
	pt2[2] = Vector2( tRangeX, -tRangeY);
	pt2[3] = Vector2(-tRangeX, -tRangeY);
	point = cpos - tpos;
	checkPt.x = point.x *  cos2 - point.y * -sin2;//逆回転はsinが反転
	checkPt.y = point.x * -sin2 + point.y *  cos2;
	if (PointInRect(checkPt, pt2))
		return true;

	return false;
}

bool Collision2DSystem::PointInRect(Vector2 point, Vector2 *rect) {
	if (point.x < rect[0].x || point.x > rect[1].x)
		return false;
	if (point.y < rect[3].y || point.y > rect[0].y)
		return false;

	return true;
}

bool Collision2DSystem::CrossLine(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4) {
	//x座標によるチェック
	if (p1.x >= p2.x) {
		if ((p1.x < p3.x && p1.x < p4.x) || (p2.x > p3.x && p2.x > p4.x))
			return false;
	}
	else {
		if ((p2.x < p3.x && p2.x < p4.x) || (p1.x > p3.x && p1.x > p4.x))
			return false;
	}
	//y座標によるチェック
	if (p1.y >= p2.y) {
		if ((p1.y < p3.y && p1.y < p4.y) || (p2.y > p3.y && p2.y > p4.y))
			return false;
	}
	else {
		if ((p2.y < p3.y && p2.y < p4.y) || (p1.y > p3.y && p1.y > p4.y))
			return false;
	}

	if (((p1.x - p2.x) * (p3.y - p1.y) + (p1.y - p2.y) * (p1.x - p3.x)) *
		((p1.x - p2.x) * (p4.y - p1.y) + (p1.y - p2.y) * (p1.x - p4.x)) > 0)
	{
		return false;
	}

	if (((p3.x - p4.x) * (p1.y - p3.y) + (p3.y - p4.y) * (p3.x - p1.x)) *
		((p3.x - p4.x) * (p2.y - p3.y) + (p3.y - p4.y) * (p3.x - p2.x)) > 0)
	{
		return false;
	}

	return true;
}

RaycastResult Collision2DSystem::RaycastCircle(Ray ray, Collider2D *col, RaycastResult result) {
	Vector2 rd = ray.end - ray.start;
	
	/// a * (t ^ 2) + b * t + c = 0
	float a = D3DXVec2Dot(&rd, &rd);
	if (a == 0) return result;

	Vector2 seg = ray.start - col->GetPosition2();
	float b = 2.f * D3DXVec2Dot(&seg, &rd);
	float c = D3DXVec2Dot(&seg, &seg) - (col->GetHalfSize().x * col->GetHalfSize().x);
	float d = (b * b) - (4 * a * c);
	if (d < 0.f) return result;

	float t1 = (-b - sqrtf(d)) / (2.f * a);
	float t2 = (-b + sqrtf(d)) / (2.f * a);
	if ((t1 < 0.f || t1 > 1.f) && (t2 < 0.f && t2 > 1.f)) return result;

	Vector2 collidedPoint;
	if ((t1 >= 0.f && t1 <= 1.f) && (t2 >= 0.f && t2 <= 1.f)) {
		collidedPoint = ray.start + rd * t1;
		Vector2 collidedPoint2 = ray.start + rd * t2;
		if (D3DXVec2Length(&(collidedPoint2 - ray.start)) < D3DXVec2Length(&(collidedPoint - ray.start)))
			collidedPoint = collidedPoint2;
	} else if (t1 >= 0.f && t1 <= 1.f) {
		collidedPoint = ray.start + rd * t1;
	} else if (t2 >= 0.f && t2 <= 1.f) {
		collidedPoint = ray.start + rd * t2;
	}
	float newLength = D3DXVec2Length(&(collidedPoint - ray.start));
	if (newLength > ray.length) return result;

	if (result.isCollided) {
		float oldLength = D3DXVec2Length(&(result.collidedPoint - ray.start));
		if (newLength < oldLength)
			result.collidedPoint = collidedPoint;
	} else {
		result.isCollided = true;
		result.collidedPoint = collidedPoint;
	}
	result.isUpdated = true;

	return result;
}

RaycastResult Collision2DSystem::RaycastRect(Ray ray, Collider2D *col, RaycastResult result) {
	Vector2 points[4];
	Ray seg;

	points[0] = Vector2(-col->GetHalfSize().x, col->GetHalfSize().y);
	points[1] = Vector2(col->GetHalfSize().x, col->GetHalfSize().y);
	points[2] = Vector2(col->GetHalfSize().x, -col->GetHalfSize().y);
	points[3] = Vector2(-col->GetHalfSize().x, -col->GetHalfSize().y);

	for (int i = 0; i < 4; i++) {
		Vector2Rotate(&points[i], D3DXToRadian(col->GetAngle()));
		points[i] += col->GetPosition2();
	}

	for (int i = 0; i < 4; i++) {
		seg = Ray(points[i], points[(i + 1) % 4]);
		result = RaycastRect(ray, seg, result);
		if (result.isUpdated) {
			result.target = col;
			result.isUpdated = false;
		}
	}

	return result;
}

RaycastResult Collision2DSystem::RaycastRect(Ray ray, Ray seg, RaycastResult result) {
	float rpx = ray.start.x;
	float rpy = ray.start.y;
	float rdx = ray.end.x - rpx;
	float rdy = ray.end.y - rpy;

	float spx = seg.start.x;
	float spy = seg.start.y;
	float sdx = seg.end.x - spx;
	float sdy = seg.end.y - spy;

	if ((rdx / ray.length == sdx / seg.length) && (rdy / ray.length == sdy / seg.length)) return result;

	/// rpx + rdx * rt = spx + sdx * st
	/// rpy + rdy * rt = spy + sdy * st
	float st = (rdx * (spy - rpy) + rdy * (rpx - spx)) / (sdx * rdy - sdy * rdx);
	float rt = (spx + sdx * st - rpx) / rdx;

	if (rt < 0.f) return result;
	if (st < 0.f || st > 1.f) return result;

	Vector2 collidedPoint = Vector2(rpx + rdx * rt, rpy + rdy * rt);
	float newLength = D3DXVec2Length(&(collidedPoint - ray.start));
	if (newLength > ray.length) return result;

	if (result.isCollided) {
		float oldLength = D3DXVec2Length(&(result.collidedPoint - ray.start));
		if (newLength < oldLength)
			result.collidedPoint = collidedPoint;
	} else {
		result.isCollided = true;
		result.collidedPoint = collidedPoint;
	}
	result.isUpdated = true;

	return result;
}

// Overlap
#pragma endregion