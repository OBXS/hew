#include "Transform.h"

Transform::Transform() {
	Reset();
	chidren = new list<Transform*>();
}

Transform::~Transform() {
	SetParent(nullptr);
	for (auto &child : *chidren)
		child->RemoveParent();
	chidren->clear();

	delete parent;
	delete chidren;
}

#pragma region Position

Vector3 Transform::GetLocalPosition() {
	return Vector3(mT._41, mT._42, mT._43);
}

Vector2 Transform::GetLocalPosition2() {
	return Vector2(mT._41, mT._42);
}

Vector3 Transform::GetPosition() {
	if (parent) {
		Vector4 position = Vector4(mT._41, mT._42, mT._43, 1.f);
		Update();
		D3DXVec4Transform(&position, &position, &parent->GetMatrix());
		return Vector3(position.x, position.y, position.z);
	} else {
		return GetLocalPosition();
	}
}

Vector2 Transform::GetPosition2() {
	return Vector2(GetPosition().x, GetPosition().y);
}

void Transform::SetLocalPositionX(float x) {
	SetLocalPosition(x, mT._42, mT._43);
}

void Transform::SetLocalPositionY(float y) {
	SetLocalPosition(mT._41, y, mT._43);
}

void Transform::SetLocalPositionZ(float z) {
	SetLocalPosition(mT._41, mT._42, z);
}

void Transform::SetLocalPosition(float x, float y) {
	SetLocalPosition(x, y, mT._43);
}

void Transform::SetLocalPosition(Vector2 v) {
	SetLocalPosition(v.x, v.y, mT._43);
}

void Transform::SetLocalPosition(float x, float y, float z) {
	D3DXMatrixTranslation(&mT, x, y, z);

	isUpdate = true;
}

void Transform::SetLocalPosition(Vector3 v) {
	SetLocalPosition(v.x, v.y, v.z);
}

void Transform::SetPositionX(float x) {
	SetPosition(x, GetPosition().y, GetPosition().z);
}

void Transform::SetPositionY(float y) {
	SetPosition(GetPosition().x, y, GetPosition().z);
}

void Transform::SetPositionZ(float z) {
	SetPosition(GetPosition().x, GetPosition().y, z);
}

void Transform::SetPosition(float x, float y) {
	SetPosition(x, y, GetPosition().z);
}

void Transform::SetPosition(Vector2 v) {
	SetPosition(v.x, v.y, GetPosition().z);
}

void Transform::SetPosition(float x, float y, float z) {
	if (parent) {
		Matrix mGT, mI;
		D3DXMatrixInverse(&mI, NULL, &mRY);
		mGT = mI;
		D3DXMatrixInverse(&mI, NULL, &mRX);
		mGT = mI * mGT;
		D3DXMatrixInverse(&mI, NULL, &mRZ);
		mGT = mI * mGT;
		D3DXMatrixInverse(&mI, NULL, &mS);
		mGT = mI * mGT;

		D3DXMatrixIdentity(&mI);
		mI._41 = x;
		mI._42 = y;
		mI._43 = z;
		mGT *= mI;

		Update();
		D3DXMatrixInverse(&mI, NULL, &parent->GetMatrix());
		mGT *= mI;

		mT._41 = mGT._41;
		mT._42 = mGT._42;
		mT._43 = mGT._43;

		isUpdate = true;
	} else {
		SetLocalPosition(x, y, z);
	}
}

void Transform::SetPosition(Vector3 v) {
	SetPosition(v.x, v.y, v.z);
}

void Transform::MoveX(float x) {
	Move(x, 0.0f, 0.0f);
}

void Transform::MoveY(float y) {
	Move(0.0f, y, 0.0f);
}

void Transform::MoveZ(float z) {
	Move(0.0f, 0.0f, z);
}

void Transform::Move(float x, float y) {
	Move(x, y, 0.0f);
}

void Transform::Move(Vector2 v) {
	Move((Vector3)v);
}

void Transform::Move(float x, float y, float z) {
	MatrixTranslate(&mT, x, y, z);

	isUpdate = true;
}

void Transform::Move(Vector3 v) {
	MatrixTranslate(&mT, v);

	isUpdate = true;
}

// Position
#pragma endregion

#pragma region Rotation

Vector3 Transform::GetLocalRotation() {
	return Vector3(D3DXToDegree(ASinCos(mRX._23, mRX._22)),
				   D3DXToDegree(ASinCos(mRY._31, mRY._33)),
				   D3DXToDegree(ASinCos(mRZ._12, mRZ._11)));
}

Vector3 Transform::GetRotation() {
	Vector3 rotation = GetLocalRotation();
	if (parent)
		rotation += parent->GetRotation();
	return rotation;
}

void Transform::SetLocalRotationX(float x) {
	D3DXMatrixRotationX(&mRX, D3DXToRadian(x));

	isUpdate = true;
}

void Transform::SetLocalRotationY(float y) {
	D3DXMatrixRotationY(&mRY, D3DXToRadian(y));

	isUpdate = true;
}

void Transform::SetLocalRotationZ(float z) {
	D3DXMatrixRotationZ(&mRZ, D3DXToRadian(z));

	isUpdate = true;
}

void Transform::SetLocalRotation(float x, float y) {
	SetLocalRotationX(x);
	SetLocalRotationY(y);
}

void Transform::SetLocalRotation(Vector2 v) {
	SetLocalRotationX(v.x);
	SetLocalRotationY(v.y);
}

void Transform::SetLocalRotation(float x, float y, float z) {
	SetLocalRotationX(x);
	SetLocalRotationY(y);
	SetLocalRotationZ(z);
}

void Transform::SetLocalRotation(Vector3 v) {
	SetLocalRotationX(v.x);
	SetLocalRotationY(v.y);
	SetLocalRotationZ(v.z);
}

void Transform::SetRotationX(float x) {
	float diff = x - GetRotation().x + GetLocalRotation().x;
	D3DXMatrixRotationX(&mRX, D3DXToRadian(diff));

	isUpdate = true;
}

void Transform::SetRotationY(float y) {
	float diff = y - GetRotation().y + GetLocalRotation().y;
	D3DXMatrixRotationY(&mRY, D3DXToRadian(diff));

	isUpdate = true;
}

void Transform::SetRotationZ(float z) {
	float diff = z - GetRotation().z + GetLocalRotation().z;
	D3DXMatrixRotationZ(&mRZ, D3DXToRadian(diff));

	isUpdate = true;
}

void Transform::SetRotation(float x, float y) {
	SetRotationX(x);
	SetRotationY(y);
}

void Transform::SetRotation(Vector2 v) {
	SetRotationX(v.x);
	SetRotationY(v.y);
}

void Transform::SetRotation(float x, float y, float z) {
	SetRotationX(x);
	SetRotationY(y);
	SetRotationZ(z);
}

void Transform::SetRotation(Vector3 v) {
	SetRotationX(v.x);
	SetRotationY(v.y);
	SetRotationZ(v.z);
}

void Transform::RotateX(float x) {
	MatrixRotateX(&mRX, D3DXToRadian(x));

	isUpdate = true;
}

void Transform::RotateY(float y) {
	MatrixRotateY(&mRY, D3DXToRadian(y));

	isUpdate = true;
}

void Transform::RotateZ(float z) {
	MatrixRotateZ(&mRZ, D3DXToRadian(z));

	isUpdate = true;
}

void Transform::Rotate(float x, float y) {
	RotateX(x);
	RotateY(y);
}

void Transform::Rotate(Vector2 v) {
	RotateX(v.x);
	RotateY(v.y);
}

void Transform::Rotate(float x, float y, float z) {
	RotateX(x);
	RotateY(y);
	RotateZ(z);
}

void Transform::Rotate(Vector3 v) {
	RotateX(v.x);
	RotateY(v.y);
	RotateZ(v.z);
}

void Transform::RotateAround(Vector3 point, Vector3 r) {
	Transform *oldParent = parent;
	Transform *center = new Transform();
	center->SetPosition(point);
	center->SetRotation(0.f, 0.f, 0.f);
	SetParent(center, true);
	center->Rotate(r);
	Transform::Update();

	Vector3 nowR = GetRotation();
	SetParent(oldParent, true);
	SetRotation(nowR);
	delete center;
}

// Rotation
#pragma endregion

#pragma region Scale

Vector3 Transform::GetLocalScale() {
	return Vector3(mS._11, mS._22, mS._33);
}

Vector3 Transform::GetScale() {
	Vector3 scale = GetLocalScale();
	if (parent)
		scale = Vector3(scale.x * parent->GetScale().x, scale.y * parent->GetScale().y, scale.z * parent->GetScale().z);
	return scale;
}

Vector2 Transform::GetLocalScale2() {
	return Vector2(mS._11, mS._22);
}

Vector2 Transform::GetScale2() {
	Vector2 scale = GetLocalScale2();
	if (parent)
		scale = Vector2(scale.x * parent->GetScale().x, scale.y * parent->GetScale().y);
	return scale;
}

void Transform::SetLocalScaleX(float x) {
	SetLocalScale(x, mS._22, mS._33);
}

void Transform::SetLocalScaleY(float y) {
	SetLocalScale(mS._11, y, mS._33);
}

void Transform::SetLocalScaleZ(float z) {
	SetLocalScale(mS._11, mS._22, z);
}

void Transform::SetLocalScale(float xy) {
	SetLocalScale(xy, xy, 1.f);
}

void Transform::SetLocalScale(float x, float y) {
	SetLocalScale(x, y, 1.f);
}

void Transform::SetLocalScale(Vector2 v) {
	SetLocalScale(v.x, v.y, 1.f);
}

void Transform::SetLocalScale(float x, float y, float z) {
	D3DXMatrixScaling(&mS, x, y, z);

	isUpdate = true;
}

void Transform::SetLocalScale(Vector3 v) {
	SetLocalScale(v.x, v.y, v.z);
}

void Transform::ScaleX(float x) {
	Scale(x, 1.f, 1.f);
}

void Transform::ScaleY(float y) {
	Scale(1.f, y, 1.f);
}

void Transform::ScaleZ(float z) {
	Scale(1.f, 1.f, z);
}

void Transform::Scale(float xy) {
	Scale(xy, xy, 1.f);
}

void Transform::Scale(float x, float y) {
	Scale(x, y, 1.f);
}

void Transform::Scale(Vector2 v) {
	Scale(v.x, v.y, 1.f);
}

void Transform::Scale(float x, float y, float z) {
	MatrixScale(&mS, x, y, z);

	isUpdate = true;
}

void Transform::Scale(Vector3 v) {
	MatrixScale(&mS, v);

	isUpdate = true;
}

// Scale
#pragma endregion

#pragma region Parent-Child

Transform *Transform::GetParent() {
	return parent;
}

void Transform::SetParent(Transform *parent) {
	if (this->parent)
		this->parent->RemoveChild(this);

	this->parent = parent;

	if (this->parent)
		this->parent->AddChild(this);
}

void Transform::SetParent(Transform *parent, bool dontMove) {
	Vector3 pos = GetPosition();
	SetParent(parent);
	if (dontMove)
		SetPosition(pos);
}

list<Transform*> *Transform::GetChidren() {
	return chidren;
}

void Transform::AddChild(Transform *child) {
	if (child)
		chidren->push_back(child);
}

void Transform::RemoveChild(Transform *child) {
	if (child)
		chidren->remove(child);
}

// Parent-Child
#pragma endregion

string Transform::GetTag() {
	return tag;
}

Matrix Transform::GetMatrix() {
	return mF;
}

Matrix Transform::GetRotationMatrix() {
	if (parent)
		return mRZ * mRX * mRY * parent->GetRotationMatrix();
	return mRZ * mRX * mRY;
}

void Transform::Reset() {
	D3DXMatrixIdentity(&mT);
	D3DXMatrixIdentity(&mRX);
	D3DXMatrixIdentity(&mRY);
	D3DXMatrixIdentity(&mRZ);
	D3DXMatrixIdentity(&mS);
	D3DXMatrixIdentity(&mTRS);
	D3DXMatrixIdentity(&mF);
}

bool Transform::Update() {
	bool isUpdated = false;
	
	if (parent) {
		if (parent->Update())
			isUpdated = true;
	}

	if (isUpdate) {
		Translate();
		isUpdated = true;
	}

	if (parent)
		mF = mTRS * parent->GetMatrix();

	return isUpdated;
}

void Transform::Translate() {
	mTRS = mS * mRZ * mRX * mRY * mT;
	mF = mTRS;
	isUpdate = false;
}

void Transform::RemoveParent() {
	Vector3 pos = GetPosition();
	parent = nullptr;
	SetPosition(pos);
}
