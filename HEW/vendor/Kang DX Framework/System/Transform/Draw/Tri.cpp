#include "Tri.h"

#define TRI_VERTEX_COUNT 3

Tri::Tri(Vector2 size, Transform *parent)
	: Tri(Vector3(0.f, 0.f, 0.f), size, Color(1.f, 1.f, 1.f, 1.f), parent) {
}

Tri::Tri(Vector2 size, Color color, Transform *parent)
	: Tri(Vector3(0.f, 0.f, 0.f), size, color, parent) {
}

Tri::Tri(Vector3 pos, Vector2 size, Transform *parent)
	: Tri(pos, size, Color(1.f, 1.f, 1.f, 1.f), parent) {
}

Tri::Tri(Vector3 pos, Vector2 size, Color color, Transform *parent) {
	vertexCount = TRI_VERTEX_COUNT;
	vertexes = new Vertex[vertexCount];
	vertexesPosition = new Vector3[vertexCount];

	float hw = size.x * 0.5f;
	float th = size.y / 3.0f;
	vertexesPosition[0] = Vector3(0.0f, th * 2.0f, 0.0f);
	vertexesPosition[1] = Vector3(hw, -th, 0.0f);
	vertexesPosition[2] = Vector3(-hw, -th, 0.0f);

	SetLocalPosition(pos);
	SetColor(color);
	SetParent(parent);
}