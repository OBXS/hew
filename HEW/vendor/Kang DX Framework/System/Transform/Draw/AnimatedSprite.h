#ifndef __ANMATED_SPRITE_H__
#define __ANMATED_SPRITE_H__

#include <map>
#include "Sprite.h"

struct SpriteAnimation {
	int index;
	bool isLoop = false;
	bool isReverse = false;
	int start;
	int end;
	int rate = 0;
};

class AnimatedSprite : public Sprite {
public:
	AnimatedSprite(string name, Vector2 splitN, Transform *parent = nullptr);
	AnimatedSprite(string name, Vector2 splitN, Vector3 pos, Transform *parent = nullptr);
	AnimatedSprite(string name, Vector2 splitN, Vector2 size, Transform *parent = nullptr);
	AnimatedSprite(string name, Vector2 splitN, Vector3 pos, Color color, Transform *parent = nullptr);
	AnimatedSprite(string name, Vector2 splitN, Vector2 size, Color color, Transform *parent = nullptr);
	AnimatedSprite(string name, Vector2 splitN, Vector3 pos, Vector2 size, Transform *parent = nullptr);
	AnimatedSprite(string name, Vector2 splitN, Vector3 pos, Vector2 size, Color color, Transform *parent = nullptr);

	// Rate: frames per second.
	void SetAnimData(int index, int row, int rate, bool isLoop = false, bool isLoopReverse = false);
	// Rate: frames per second.
	void SetAnimData(int index, Vector2 start, Vector2 end, int rate, bool isLoop = false, bool isLoopReverse = false);
	void SwitchAnim(int index);
	void PlayAnim(int index);
	bool IsPlaying();
	void Draw();

private:
	Vector2 splitN;
	map<int, SpriteAnimation> *animData;

	SpriteAnimation *anim = nullptr;
	int frame = 0;
	float frameTimer = 0.0f;
	bool playReverse = false;
	Vector2 lastFrame = Vector2(0.f, 0.f);
};

#endif