#ifndef __RECT_H__
#define __RECT_H__

#include "Shape.h"

class Rect : public Shape {
public:
	Rect(Vector2 size, Transform *parent = nullptr);
	Rect(Vector2 size, float cr, Transform *parent = nullptr);
	Rect(Vector2 size, Color color, Transform *parent = nullptr);
	Rect(Vector2 size, float cr, Color color, Transform *parent = nullptr);
	Rect(Vector3 pos, Vector2 size, Transform *parent = nullptr);
	Rect(Vector3 pos, Vector2 size, float cr, Transform *parent = nullptr);
	Rect(Vector3 pos, Vector2 size, Color color, Transform *parent = nullptr);
	Rect(Vector3 pos, Vector2 size, float cr, Color color, Transform *parent = nullptr);
};

#endif