#ifndef __SPRITE_H__
#define __SPRITE_H__

#include "..\..\TextureManager.h"
#include "Drawing.h"

class Sprite : public Drawing {
public:
	Sprite(string name, Transform *parent = nullptr);
	Sprite(string name, Vector3 pos, Transform *parent = nullptr);
	Sprite(string name, Vector3 pos, Color color, Transform *parent = nullptr);
	Sprite(string name, Vector2 size, Transform *parent = nullptr);
	Sprite(string name, Vector2 size, Color color, Transform *parent = nullptr);
	Sprite(string name, Vector2 size, Color color, Vector2 cutPos, Transform *parent = nullptr);
	Sprite(string name, Vector2 size, Color color, Vector4 cutRange, Transform *parent = nullptr);
	Sprite(string name, Vector3 pos, Vector2 size, Transform *parent = nullptr);
	Sprite(string name, Vector3 pos, Vector2 size, Vector2 cutPos, Transform *parent = nullptr);
	Sprite(string name, Vector3 pos, Vector2 size, Vector4 cutRange, Transform *parent = nullptr);
	Sprite(string name, Vector3 pos, Vector2 size, Color color, Transform *parent = nullptr);
	Sprite(string name, Vector3 pos, Vector2 size, Color color, Vector2 cutPos = Vector2(0.f, 0.f), Transform *parent = nullptr);
	Sprite(string name, Vector3 pos, Vector2 size, Color color, Vector4 cutRange, Transform *parent = nullptr);
	~Sprite();
	
	Vector2 GetSize();
	Vector2 GetTextureSize();
	Color GetColor();
	float GetAlpha();
	void SetColor(float r, float g, float b);
	void SetColor(float r, float g, float b, float a);
	void SetColor(Color color);
	void SetAlpha(float a);

	// UV

	// Cut with this sprite's size.
	void SetCut(Vector2 pos);
	// range: Vector4(posX, posY, width, height).
	void SetCut(Vector4 range);
	void SetCut(float x, float y, float w, float h);
	// uv: 0.f ~ 1.f.
	void SetUV(Vector4 uv);
	// uv: 0.f ~ 1.f.
	void SetUV(float x, float y, float w, float h);

	// Draw

	void Draw();
	void DrawByCutting(float x, float y, float w, float h);
	// uv: 0.f ~ 1.f.
	void DrawByUV(float x, float y, float w, float h);

private:
	struct Vertex {
		Vector3 position;
		D3DCOLOR color;
		Vector2 uv;
	};
	Vertex *vertexes;

	Vector3 *vertexesPosition;
	Texture *texture;
	Vector2 size;
};

#endif