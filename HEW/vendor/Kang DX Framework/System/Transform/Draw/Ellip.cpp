#include "Ellip.h"

#define VERTEX_POW  0.8f

Ellip::Ellip(float size, Transform *parent)
	: Ellip(Vector3(0.f, 0.f, 0.f), Vector2(size, size), Color(1.f, 1.f, 1.f, 1.f), parent) {
}

Ellip::Ellip(Vector2 size, Transform *parent)
	: Ellip(Vector3(0.f, 0.f, 0.f), size, Color(1.f, 1.f, 1.f, 1.f), parent) {
}

Ellip::Ellip(float size, Color color, Transform *parent)
	: Ellip(Vector3(0.f, 0.f, 0.f), Vector2(size, size), color, parent) {
}

Ellip::Ellip(Vector2 size, Color color, Transform *parent)
	: Ellip(Vector3(0.f, 0.f, 0.f), size, color, parent) {
}

Ellip::Ellip(Vector3 pos, float size, Transform *parent)
	: Ellip(pos, Vector2(size, size), Color(1.f, 1.f, 1.f, 1.f), parent) {
}

Ellip::Ellip(Vector3 pos, Vector2 size, Transform *parent)
	: Ellip(pos, size, Color(1.f, 1.f, 1.f, 1.f), parent) {
}

Ellip::Ellip(Vector3 pos, float size, Color color, Transform *parent)
	: Ellip(pos, Vector2(size, size), color, parent) {
}

Ellip::Ellip(Vector3 pos, Vector2 size, Color color, Transform *parent) {
	float avgSize = (size.x + size.y) * 0.5f;
	vertexCount = (int)powf(avgSize, VERTEX_POW) + 4;
	vertexes = new Vertex[vertexCount];
	vertexesPosition = new Vector3[vertexCount];

	float hw = size.x * 0.5f;
	float hh = size.y * 0.5f;
	for (int i = 0; i < vertexCount; i++) {
		int n = (i % 2 == 0) ? vertexCount - i / 2 : (i - 1) / 2;
		float xd = hw * cosf(TWO_PI * n / vertexCount);
		float yd = hh * sinf(TWO_PI * n / vertexCount);

		vertexesPosition[i] = Vector3(xd, yd, 0.0f);
	}

	SetLocalPosition(pos);
	SetColor(color);
	SetParent(parent);
}