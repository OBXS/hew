#include "Rect.h"

#define RECT_VERTEX_COUNT 4
#define VERTEX_POW  0.8f

Rect::Rect(Vector2 size, Transform *parent)
	: Rect(Vector3(0.f, 0.f, 0.f), size, 0.f, Color(1.f, 1.f, 1.f, 1.f), parent) {
}

Rect::Rect(Vector2 size, float cr, Transform *parent)
	: Rect(Vector3(0.f, 0.f, 0.f), size, cr, Color(1.f, 1.f, 1.f, 1.f), parent) {
}

Rect::Rect(Vector2 size, Color color, Transform *parent)
	: Rect(Vector3(0.f, 0.f, 0.f), size, 0.f, color, parent) {
}

Rect::Rect(Vector2 size, float cr, Color color, Transform *parent)
	: Rect(Vector3(0.f, 0.f, 0.f), size, cr, color, parent) {
}

Rect::Rect(Vector3 pos, Vector2 size, Transform *parent)
	: Rect(pos, size, 0.f, Color(1.f, 1.f, 1.f, 1.f), parent) {
}

Rect::Rect(Vector3 pos, Vector2 size, float cr, Transform *parent)
	: Rect(pos, size, cr, Color(1.f, 1.f, 1.f, 1.f), parent) {
}

Rect::Rect(Vector3 pos, Vector2 size, Color color, Transform *parent)
	: Rect(pos, size, 0.f, color, parent) {
}

Rect::Rect(Vector3 pos, Vector2 size, float cr, Color color, Transform *parent) {
	if (cr <= 0) {
		vertexCount = RECT_VERTEX_COUNT;
	} else {
		float avgSize = (size.x + size.y) * 0.5f;
		vertexCount = (int)powf(avgSize, VERTEX_POW) + RECT_VERTEX_COUNT;
		if (vertexCount % RECT_VERTEX_COUNT != 0)
			vertexCount += RECT_VERTEX_COUNT - (vertexCount % RECT_VERTEX_COUNT);
	}
	vertexes = new Vertex[vertexCount];
	vertexesPosition = new Vector3[vertexCount];

	float hw = size.x * 0.5f;
	float hh = size.y * 0.5f;

	if (cr <= 0) {
		vertexesPosition[0] = Vector3(-hw, hh, 0.0f);
		vertexesPosition[1] = Vector3(hw, hh, 0.0f);
		vertexesPosition[2] = Vector3(-hw, -hh, 0.0f);
		vertexesPosition[3] = Vector3(hw, -hh, 0.0f);
	} else {
		int ellipCount = max(vertexCount - RECT_VERTEX_COUNT, RECT_VERTEX_COUNT);
		int edgeCount = 0;

		for (int i = 0; i < vertexCount; i++) {
			int n = ((i - edgeCount) % 2 == 1) ? (ellipCount - 1) - ((i - edgeCount) / 2) : (i - edgeCount) / 2;
			float xd = cr * cosf(TWO_PI * n / ellipCount) + ((i < vertexCount / 2) ? hw - cr : -hw + cr);
			float yd = cr * sinf(TWO_PI * n / ellipCount) + ((i % 2 == 1) ? hh - cr : -hh + cr);
			if (i == 0 || i == vertexCount - 1) {
				edgeCount++;
			} else if (i == vertexCount / 2 - 1) {
				edgeCount += 2;
			}

			vertexesPosition[i] = Vector3(xd, yd, 0.0f);
		}
	}

	SetLocalPosition(pos);
	SetColor(color);
	SetParent(parent);
}