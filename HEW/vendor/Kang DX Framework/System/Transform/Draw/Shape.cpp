#include "Shape.h"

Shape::Shape() {}

Shape::~Shape() {
	delete[] vertexes;
	delete[] vertexesPosition;
}

Color Shape::GetColor() {
	return (vertexes == nullptr) ? Color(1.f, 1.f, 1.f, 1.f) : Color(vertexes[0].color);
}

float Shape::GetAlpha() {
	return (vertexes == nullptr) ? 1.f : Color(vertexes[0].color).a;
}

void Shape::SetColor(float r, float g, float b) {
	SetColor(Color(r, g, b, GetAlpha()));
}

void Shape::SetColor(float r, float g, float b, float a) {
	SetColor(Color(r, g, b, a));
}

void Shape::SetColor(Color color) {
	for (int i = 0; i < vertexCount; i++)
		vertexes[i].color = color;
}

void Shape:: SetAlpha(float a) {
	SetColor(0x00FFFFFF & vertexes[0].color | ((DWORD)(a * 255.f) << 24));
}

void Shape::Draw() {
	Update();

	if (isHidden) return;

	for (int i = 0; i < vertexCount; i++)
		D3DXVec3TransformCoord(&vertexes[i].position, &vertexesPosition[i], &mF);
	
	GetDevice()->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, vertexCount - 2, vertexes, sizeof(Vertex));
}