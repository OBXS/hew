#ifndef __DRAWING_H__
#define __DRAWING_H__

#include "..\Transform.h"

class Drawing : public Transform {
public:
	bool isHidden = false;

	virtual Color GetColor() = 0;
	virtual float GetAlpha() = 0;
	virtual void SetColor(float r, float g, float b) = 0;
	virtual void SetColor(float r, float g, float b, float a) = 0;
	virtual void SetColor(Color color) = 0;
	virtual void SetAlpha(float a) = 0;

protected:
	Drawing() {};
};

#endif