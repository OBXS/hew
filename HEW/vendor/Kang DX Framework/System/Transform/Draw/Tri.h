#ifndef __TRI_H__
#define __TRI_H__

#include "Shape.h"

class Tri : public Shape {
public:
	Tri(Vector2 size, Transform *parent = nullptr);
	Tri(Vector2 size, Color color, Transform *parent = nullptr);
	Tri(Vector3 pos, Vector2 size, Transform *parent = nullptr);
	Tri(Vector3 pos, Vector2 size, Color color, Transform *parent = nullptr);
};

#endif