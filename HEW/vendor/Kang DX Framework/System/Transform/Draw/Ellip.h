#ifndef __ELLIP_H__
#define __ELLIP_H__

#include "Shape.h"

class Ellip : public Shape {
public:
	Ellip(float size, Transform *parent = nullptr);
	Ellip(Vector2 size, Transform *parent = nullptr);
	Ellip(float size, Color color, Transform *parent = nullptr);
	Ellip(Vector2 size, Color color, Transform *parent = nullptr);
	Ellip(Vector3 pos, float size, Transform *parent = nullptr);
	Ellip(Vector3 pos, Vector2 size, Transform *parent = nullptr);
	Ellip(Vector3 pos, float size, Color color, Transform *parent = nullptr);
	Ellip(Vector3 pos, Vector2 size, Color color, Transform *parent = nullptr);
};

#endif
