#include "AnimatedSprite.h"

AnimatedSprite::AnimatedSprite(string name, Vector2 splitN, Transform *parent)
	: AnimatedSprite(name, splitN, Vector3(0.f, 0.f, 0.f), Vector2(TextureManager::Get(name)->size.x / splitN.x, TextureManager::Get(name)->size.y / splitN.y), Color(1.f, 1.f, 1.f, 1.f), parent) {
}

AnimatedSprite::AnimatedSprite(string name, Vector2 splitN, Vector3 pos, Transform *parent)
	: AnimatedSprite(name, splitN, pos, Vector2(TextureManager::Get(name)->size.x / splitN.x, TextureManager::Get(name)->size.y / splitN.y), Color(1.f, 1.f, 1.f, 1.f), parent) {
}

AnimatedSprite::AnimatedSprite(string name, Vector2 splitN, Vector2 size, Transform *parent)
	: AnimatedSprite(name, splitN, Vector3(0.f, 0.f, 0.f), size, Color(1.f, 1.f, 1.f, 1.f), parent) {
}

AnimatedSprite::AnimatedSprite(string name, Vector2 splitN, Vector3 pos, Color color, Transform *parent)
	: AnimatedSprite(name, splitN, pos, Vector2(TextureManager::Get(name)->size.x / splitN.x, TextureManager::Get(name)->size.y / splitN.y), color, parent) {
}

AnimatedSprite::AnimatedSprite(string name, Vector2 splitN, Vector2 size, Color color, Transform *parent)
	: AnimatedSprite(name, splitN, Vector3(0.f, 0.f, 0.f), size, color, parent) {
}

AnimatedSprite::AnimatedSprite(string name, Vector2 splitN, Vector3 pos, Vector2 size, Transform *parent)
	: AnimatedSprite(name, splitN, pos, size, Color(1.f, 1.f, 1.f, 1.f), parent) {
}

AnimatedSprite::AnimatedSprite(string name, Vector2 splitN, Vector3 pos, Vector2 size, Color color, Transform *parent)
	: Sprite(name, pos, size, color, parent) {

	this->splitN = splitN;
	animData = new map<int, SpriteAnimation>();
}

void AnimatedSprite::SetAnimData(int index, int row, int rate, bool isLoop, bool isLoopReverse) {
	SetAnimData(index, Vector2(0.f, (float)row), Vector2(splitN.x, (float)row), rate, isLoop, isLoopReverse);
}

void AnimatedSprite::SetAnimData(int index, Vector2 start, Vector2 end, int rate, bool isLoop, bool isLoopReverse) {
	SpriteAnimation anim;
	anim.index = index;
	anim.start = (int)(start.y * splitN.x + start.x);
	anim.end = (int)(end.y * splitN.x + end.x);
	anim.rate = rate;
	anim.isLoop = isLoop;
	anim.isReverse = isLoopReverse;
	(*animData)[index] = anim;
}

void AnimatedSprite::SwitchAnim(int index) {
	if (anim && anim->index == index) return;
	PlayAnim(index);
}

void AnimatedSprite::PlayAnim(int index) {
	anim = &(*animData)[index];
	frameTimer = 0.f;
	playReverse = false;
}

bool AnimatedSprite::IsPlaying() {
	return anim ? true : false;
}

void AnimatedSprite::Draw() {
	if (anim) {
		int frame = anim->start + (int)(frameTimer * anim->rate);
		lastFrame = Vector2(GetTextureSize().x * (frame % (int)splitN.x / splitN.x), GetTextureSize().y * (frame / (int)splitN.x / splitN.y));
		SetCut(lastFrame);
		
		frameTimer += Time::GetDeltaTime() * (playReverse ? -1.0f : 1.0f);
		if (anim->start + (int)(frameTimer * anim->rate) >= anim->end + 1) {
			if (anim->isLoop) {
				if (anim->isReverse) {
					frameTimer -= 1.f / anim->rate;
					playReverse = true;
				} else {
					frameTimer = Time::GetDeltaTime();
				}
			} else {
				anim = nullptr;
			}
		} else if (frameTimer <= 0.0f) {
			frameTimer += 1.f / anim->rate;
			playReverse = false;
		}
	} else {
		SetCut(lastFrame);
	}
	
	Sprite::Draw();
}