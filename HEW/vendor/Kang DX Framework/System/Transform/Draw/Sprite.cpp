#include "Sprite.h"

#define VERTEX_COUNT 4

Sprite::Sprite(string name, Transform *parent)
	: Sprite(name, Vector3(0.f, 0.f, 0.f), TextureManager::Get(name)->size, Color(1.f, 1.f, 1.f, 1.f), Vector4(0.f, 0.f, TextureManager::Get(name)->size.x, TextureManager::Get(name)->size.y), parent) {
}

Sprite::Sprite(string name, Vector3 pos, Transform *parent)
	: Sprite(name, pos, TextureManager::Get(name)->size, Color(1.f, 1.f, 1.f, 1.f), Vector4(0.f, 0.f, TextureManager::Get(name)->size.x, TextureManager::Get(name)->size.y), parent) {
}

Sprite::Sprite(string name, Vector3 pos, Color color = Color(1.f, 1.f, 1.f, 1.f), Transform *parent)
	: Sprite(name, pos, TextureManager::Get(name)->size, color, Vector4(0.f, 0.f, TextureManager::Get(name)->size.x, TextureManager::Get(name)->size.y), parent) {
}

Sprite::Sprite(string name, Vector2 size, Transform *parent)
	: Sprite(name, Vector3(0.f, 0.f, 0.f), size, Color(1.f, 1.f, 1.f, 1.f), Vector4(0.f, 0.f, size.x, size.y), parent) {
}

Sprite::Sprite(string name, Vector2 size, Color color, Transform *parent)
	: Sprite(name, Vector3(0.f, 0.f, 0.f), size, color, Vector4(0.f, 0.f, size.x, size.y), parent) {
}

Sprite::Sprite(string name, Vector2 size, Color color, Vector2 cutPos, Transform *parent)
	: Sprite(name, Vector3(0.f, 0.f, 0.f), size, color, Vector4(cutPos.x, cutPos.y, size.x, size.y), parent) {
}

Sprite::Sprite(string name, Vector2 size, Color color, Vector4 cutRange, Transform *parent)
	: Sprite(name, Vector3(0.f, 0.f, 0.f), size, color, cutRange, parent) {
}

Sprite::Sprite(string name, Vector3 pos, Vector2 size, Transform *parent)
	: Sprite(name, pos, size, Color(1.f, 1.f, 1.f, 1.f), Vector4(0.f, 0.f, size.x, size.y), parent) {
}

Sprite::Sprite(string name, Vector3 pos, Vector2 size, Vector2 cutPos, Transform *parent)
	: Sprite(name, pos, size, Color(1.f, 1.f, 1.f, 1.f), Vector4(cutPos.x, cutPos.y, size.x, size.y), parent) {
}

Sprite::Sprite(string name, Vector3 pos, Vector2 size, Vector4 cutRange, Transform *parent)
	: Sprite(name, pos, size, Color(1.f, 1.f, 1.f, 1.f), cutRange, parent) {
}

Sprite::Sprite(string name, Vector3 pos, Vector2 size, Color color, Transform *parent)
	: Sprite(name, pos, size, color, Vector4(0.f, 0.f, size.x, size.y), parent) {
}

Sprite::Sprite(string name, Vector3 pos, Vector2 size, Color color, Vector2 cutPos, Transform *parent)
	: Sprite(name, pos, size, color, Vector4(cutPos.x, cutPos.y, size.x, size.y), parent) {
}

Sprite::Sprite(string name, Vector3 pos, Vector2 size, Color color, Vector4 cutRange, Transform *parent) {
	texture = TextureManager::Get(name);

	vertexes = new Vertex[VERTEX_COUNT];
	vertexesPosition = new Vector3[VERTEX_COUNT];

	this->size = size;
	float hw = size.x * 0.5f;
	float hh = size.y * 0.5f;
	vertexesPosition[0] = Vector3(-hw, hh, 0.f);
	vertexesPosition[1] = Vector3(hw, hh, 0.f);
	vertexesPosition[2] = Vector3(-hw, -hh, 0.f);
	vertexesPosition[3] = Vector3(hw, -hh, 0.f);

	SetLocalPosition(pos);
	SetColor(color);
	SetCut(cutRange);
	SetParent(parent);
}

Sprite::~Sprite() {
	delete[] vertexes;
	delete[] vertexesPosition;
}

Vector2 Sprite::GetSize() {
	return size;
}

Vector2 Sprite::GetTextureSize() {
	return texture->size;
}

Color Sprite::GetColor() {
	return (vertexes == nullptr) ? Color(1.f, 1.f, 1.f, 1.f) : Color(vertexes[0].color);
}

float Sprite::GetAlpha() {
	return (vertexes == nullptr) ? 1.f : Color(vertexes[0].color).a;
}

void Sprite::SetColor(float r, float g, float b) {
	SetColor(Color(r, g, b, GetAlpha()));
}

void Sprite::SetColor(float r, float g, float b, float a) {
	SetColor(Color(r, g, b, a));
}

void Sprite::SetColor(Color color) {
	for (int i = 0; i < VERTEX_COUNT; i++)
		vertexes[i].color = color;
}

void Sprite::SetAlpha(float a) {
	SetColor(0x00FFFFFF & vertexes[0].color | ((DWORD)(a * 255.f) << 24));
}

void Sprite::SetCut(Vector2 pos) {
	SetCut(pos.x, pos.y, size.x, size.y);
}

void Sprite::SetCut(Vector4 range) {
	SetCut(range.x, range.y, range.z, range.w);
}

void Sprite::SetCut(float x, float y, float w, float h) {
	x /= texture->size.x;
	y /= texture->size.y;
	w /= texture->size.x;
	h /= texture->size.y;
	SetUV(x, y, w, h);
}

void Sprite::SetUV(Vector4 uv) {
	SetCut(uv.x, uv.y, uv.z, uv.w);
}

void Sprite::SetUV(float x, float y, float w, float h) {
	vertexes[0].uv = Vector2(x, y);
	vertexes[1].uv = Vector2(x + w, y);
	vertexes[2].uv = Vector2(x, y + h);
	vertexes[3].uv = Vector2(x + w, y + h);
}

void Sprite::Draw() {
	Update();

	if (isHidden) return;

	for (int i = 0; i < VERTEX_COUNT; i++)
		D3DXVec3TransformCoord(&vertexes[i].position, &vertexesPosition[i], &mF);

	GetDevice()->SetTexture(0, texture->texture);
	GetDevice()->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, VERTEX_COUNT - 2, vertexes, sizeof(Vertex));
	GetDevice()->SetTexture(0, nullptr);
}

void Sprite::DrawByCutting(float x, float y, float w, float h) {
	SetCut(x, y, w, h);
	Draw();
	SetCut(0.f, 0.f, texture->size.x, texture->size.y);
}

void Sprite::DrawByUV(float x, float y, float w, float h) {
	SetUV(x, y, w, h);
	Draw();
	SetUV(0.f, 0.f, 1.f, 1.f);
}