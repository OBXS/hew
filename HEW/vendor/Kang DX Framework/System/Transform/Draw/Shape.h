#ifndef __SHAPE_H__
#define __SHAPE_H__

#include "Drawing.h"

class Shape : public Drawing {
public:
	~Shape();

	Color GetColor();
	float GetAlpha();
	void SetColor(float r, float g, float b);
	void SetColor(float r, float g, float b, float a);
	void SetColor(Color color);
	void SetAlpha(float a);
	void Draw();

protected:
	int vertexCount;

	struct Vertex {
		Vector3 position;
		D3DCOLOR color;
	};
	Vertex *vertexes;

	Vector3 *vertexesPosition;

	Shape();
};

#endif