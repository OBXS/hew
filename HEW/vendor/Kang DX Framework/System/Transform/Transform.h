#ifndef __TRANSFORM_H__
#define __TRANSFORM_H__

#include <list>
#include "..\Global.h"

using namespace std;

class Transform {
public:
	Transform();
	~Transform();

	// Position

	Vector3 GetLocalPosition();
	Vector2 GetLocalPosition2();
	Vector3 GetPosition();
	Vector2 GetPosition2();

	void SetLocalPositionX(float x);
	void SetLocalPositionY(float y);
	void SetLocalPositionZ(float z);
	void SetLocalPosition(float x, float y);
	void SetLocalPosition(Vector2 v);
	void SetLocalPosition(float x, float y, float z);
	void SetLocalPosition(Vector3 v);

	void SetPositionX(float x);
	void SetPositionY(float y);
	void SetPositionZ(float z);
	void SetPosition(float x, float y);
	void SetPosition(Vector2 v);
	void SetPosition(float x, float y, float z);
	void SetPosition(Vector3 v);

	void MoveX(float x);
	void MoveY(float y);
	void MoveZ(float z);
	void Move(float x, float y);
	void Move(Vector2 v);
	void Move(float x, float y, float z);
	void Move(Vector3 v);

	// Rotation

	Vector3 GetLocalRotation();
	Vector3 GetRotation();

	void SetLocalRotationX(float x);
	void SetLocalRotationY(float y);
	void SetLocalRotationZ(float z);
	void SetLocalRotation(float x, float y);
	void SetLocalRotation(Vector2 v);
	void SetLocalRotation(float x, float y, float z);
	void SetLocalRotation(Vector3 v);

	void SetRotationX(float x);
	void SetRotationY(float y);
	void SetRotationZ(float z);
	void SetRotation(float x, float y);
	void SetRotation(Vector2 v);
	void SetRotation(float x, float y, float z);
	void SetRotation(Vector3 v);

	void RotateX(float x);
	void RotateY(float y);
	void RotateZ(float z);
	void Rotate(float x, float y);
	void Rotate(Vector2 v);
	void Rotate(float x, float y, float z);
	void Rotate(Vector3 v);
	void RotateAround(Vector3 point, Vector3 r);

	Vector3 GetLocalScale();
	Vector3 GetScale();
	Vector2 GetLocalScale2();
	Vector2 GetScale2();

	// Scale

	void SetLocalScaleX(float x);
	void SetLocalScaleY(float y);
	void SetLocalScaleZ(float z);
	void SetLocalScale(float xy);
	void SetLocalScale(float x, float y);
	void SetLocalScale(Vector2 v);
	void SetLocalScale(float x, float y, float z);
	void SetLocalScale(Vector3 v);

	void ScaleX(float x);
	void ScaleY(float y);
	void ScaleZ(float z);
	void Scale(float xy);
	void Scale(float x, float y);
	void Scale(Vector2 v);
	void Scale(float x, float y, float z);
	void Scale(Vector3 v);
	
	// Parent-Child

	Transform *GetParent();
	void SetParent(Transform *parent);
	void SetParent(Transform *parent, bool dontMove);
	list<Transform*> *GetChidren();
	void AddChild(Transform *child);
	void RemoveChild(Transform *child);

	string GetTag();
	Matrix GetMatrix();
	Matrix GetRotationMatrix();
	void Reset();
	bool Update();

protected:
	string tag = "";
	Transform *parent = nullptr;
	Matrix mT, mRX, mRY, mRZ, mS, mTRS, mF;
	bool isUpdate = false;

	void Translate();

private:
	list<Transform*> *chidren;

	void RemoveParent();
};

#endif