// SceneManager.h
// シーンマネージャー
//
// Manage scene objects. Call the functions by class name.
// シーンオブジェクトを管理する。クラス名から関数を呼び出す。

#ifndef __SCENE_MANAGER_H__
#define __SCENE_MANAGER_H__

#include <map>
#include "Scene.h"

using namespace std;

class SceneManager {
public:
	static void AddScene(string name, Scene * scene);
	static void RemoveScene(string name);
	static void ChangeScene(string name);
	static Scene *GetNowScene();
	static string GetNowSceneName();
	static void Update();
	static void Draw();

private:
	static map<string, Scene*> *sceneList;
	static Scene *scene;
	static string name;
};

#endif