// Texture.h
// テクスチャ
//
// Texture structure.
// テクスチャ構造体。

#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include <iostream>
#include "Global.h"

using namespace std;

struct Texture {
	string name;
	Vector2 size;
	LPDIRECT3DTEXTURE9 texture;
};

#endif