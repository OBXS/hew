// TextureManager.h
// テクスチャマネージャー
//
// Manage textures. Call the functions by class name.
// テクスチャを管理する。クラス名から関数を呼び出す。

#ifndef __TEXTURE_MANAGER_H__
#define __TEXTURE_MANAGER_H__

#include <map>
#include "Texture.h"

class TextureManager {
public:
	// Only for POT texture without setting size.
	static Texture *Load(string fileName);
	static Texture *Load(string fileName, Vector2 size);
	static Texture *Get(string name);
	static void Release(string name);

private:
	static map<string, Texture*> *textureList;

	static Texture *Load(string fileName, Vector2 size, bool isPOTSize);
};

#endif