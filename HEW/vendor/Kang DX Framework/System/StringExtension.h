#ifndef __STRING_EXTENSION_H__
#define __STRING_EXTENSION_H__

#include <iostream>
#include <Windows.h>

using namespace std;

static HWND g_hWnd = nullptr;

inline void RemoveExtension(string &fileName) {
	int extension = fileName.find('.');
	if (extension != -1)
		fileName.erase(extension);
}

inline void ErrorMessage(string message) {
	MessageBox(g_hWnd, message.c_str(), "Error", MB_ICONASTERISK);
}

inline void ErrorMessage(string message, string objectName) {
	message.append(objectName);
	message.append(".\"");
	MessageBox(g_hWnd, message.c_str(), "Error", MB_ICONASTERISK);
}

#endif