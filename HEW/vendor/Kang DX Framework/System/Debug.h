#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "Global.h"

void DebugPrintf(int n);
void DebugPrintf(float f);
void DebugPrintf(Vector2 v);
void DebugPrintf(Vector3 v);
void DebugPrintf(Matrix m);
void DebugPrintf(const char *pFormat, ...);

void InitDebugFont(void);
void FinDebugFont(void);
void DebugFontDraw(int x, int y, const char* pFormat, ...);

#endif