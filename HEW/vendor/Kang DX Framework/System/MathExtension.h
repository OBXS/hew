#ifndef __MATH_EXTENSION_H__
#define __MATH_EXTENSION_H__

#include <d3dx9.h>

#define HALF_PI (D3DX_PI * 0.5f)
#define TWO_PI (D3DX_PI * 2.0f)

typedef D3DXVECTOR2 Vector2;
typedef D3DXVECTOR3 Vector3;
typedef D3DXVECTOR4 Vector4;
typedef D3DXMATRIX Matrix;
typedef D3DXCOLOR Color;

inline float Clamp(float n, float min, float max) {
	if (n < min) return min;
	if (n > max) return max;
	return n;
}

inline float ASinCos(float sin, float cos) {
	float as = asinf(sin);
	float ac = acosf(cos);
	
	if (sin < 0.0f && cos < 0.0f) {
		return -ac;
	} else if (sin < 0.0f) {
		return as;
	} else if (cos < 0.0f) {
		return ac;
	} else {
		return as;
	}
}

inline void Vector2Rotate(Vector2 *vOut, float angle) {
	float x = cosf(angle) * vOut->x - sinf(angle) * vOut->y;
	float y = sinf(angle) * vOut->x + cosf(angle) * vOut->y;

	vOut->x = x;
	vOut->y = y;
}

inline void MatrixTranslate(Matrix *mOut, float x, float y, float z) {
	mOut->_41 += x;
	mOut->_42 += y;
	mOut->_43 += z;
}

inline void MatrixTranslate(Matrix *mOut, Vector3 v) {
	mOut->_41 += v.x;
	mOut->_42 += v.y;
	mOut->_43 += v.z;
}

inline void MatrixRotateX(Matrix *mOut, float x) {
	float r = ASinCos(mOut->_23, mOut->_22);
	
	mOut->_22 = cosf(x + r);
	mOut->_23 = sinf(x + r);
	mOut->_32 = -mOut->_23;
	mOut->_33 = mOut->_22; 
}

inline void MatrixRotateY(Matrix *mOut, float y) {
	float r = ASinCos(mOut->_31, mOut->_33);

	mOut->_11 = cosf(y + r);
	mOut->_13 = -sinf(y + r);
	mOut->_31 = -mOut->_13;
	mOut->_33 = mOut->_11;
}

inline void MatrixRotateZ(Matrix *mOut, float z) {
	float r = ASinCos(mOut->_12, mOut->_11);

	mOut->_11 = cosf(z + r);
	mOut->_12 = sinf(z + r);
	mOut->_21 = -mOut->_12;
	mOut->_22 = mOut->_11;
}

inline void MatrixScale(Matrix *mOut, float x, float y, float z) {
	mOut->_11 *= x;
	mOut->_22 *= y;
	mOut->_33 *= z;
}

inline void MatrixScale(Matrix *mOut, Vector3 v) {
	mOut->_11 *= v.x;
	mOut->_22 *= v.y;
	mOut->_33 *= v.z;
}

#endif