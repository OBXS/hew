#ifndef __INPUT_BUTTON_H__
#define __INPUT_BUTTON_H__

enum class InputButton {
	PadUp = 0x00000001,
	PadDown = 0x00000002,
	PadLeft = 0x00000004,
	PadRight = 0x00000008,

	Start = 0x00000010,
	Back = 0x00000020,
	LeftStick = 0x00000040,
	RightStick = 0x00000080,

	LB = 0x00000100,
	RB = 0x00000200,

	A = 0x00001000,
	B = 0x00002000,
	X = 0x00004000,
	Y = 0x00008000,

	TriggerLeft = 0x00010000,
	TriggerRight = 0x00020000,

	StickLeftUp = 0x00100000,
	StickLeftDown = 0x00200000,
	StickLeftLeft = 0x00400000,
	StickLeftRight = 0x00800000,

	StickRightUp = 0x01000000,
	StickRightDown = 0x02000000,
	StickRightLeft = 0x04000000,
	StickRightRight = 0x08000000,

	StickLeftVertical = StickLeftUp,
	StickLeftHorizontal = StickLeftRight,
	StickRightVertical = StickRightUp,
	StickRightHorizontal = StickRightRight
};

enum class MouseButton {
	Left,
	Right,
	Middle
};

#endif