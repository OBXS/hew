#ifndef __SCREEN_FADER_H__
#define __SCREEN_FADER_H__

#include "Transform.h"

class ScreenFader {
public:
	static void Draw();

	static void Fade(float duration, Color start, Color end);
	static bool IsActive();
	static void SetCamera(Transform *camera);

private:
	static Rect *image;
	static float timer;
	static float duration;
	static Color colorStart;
	static Color colorEnd;
};

#endif