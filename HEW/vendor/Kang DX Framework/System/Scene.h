// Scene.h
// シーン
//
// Base class that is used for scene manager. Inherit it.
// シーンマネージャーで管理できるの基底クラス。継承して使う。

#ifndef __SCENE_H__
#define __SCENE_H__

class Scene {
public:
	virtual void Enter() {}
	virtual void Update() {}
	virtual void Draw() {}
	virtual void Exit() {}

protected:
	Scene() {}
};

#endif