#include "Camera.h"
#include "ScreenFader.h"

Camera::Camera() {
	eye = Vector3(0.f, 0.f, -1500.f);
	at = Vector3(0.f, 0.f, 0.f);
	up = Vector3(0.f, 1.f, 0.f);

	ScreenFader::SetCamera(this);
}

void Camera::Update() {
	if (!Transform::Update()) return;

	Vector3 eye, at, up;
	Matrix mVP;

	D3DXVec3TransformCoord(&eye, &this->eye, &GetMatrix());
	D3DXVec3TransformCoord(&at, &this->at, &GetMatrix());
	D3DXVec3TransformCoord(&up, &this->up, &GetRotationMatrix());

	D3DXMatrixLookAtLH(&mVP, &eye, &at, &up);
	GetDevice()->SetTransform(D3DTS_VIEW, &mVP);

	D3DXMatrixOrthoLH(&mVP, SCREEN_W * GetScale().x, SCREEN_H * GetScale().y, 0.f, 3000.f);
	GetDevice()->SetTransform(D3DTS_PROJECTION, &mVP);
}

Vector2 Camera::InputToCamera(Vector2 input) {
	return  Vector2((input.x - SCREEN_W * 0.5f + GetPosition().x) * GetScale().x, (SCREEN_H * 0.5f - input.y + GetPosition().y) * GetScale().y);
}
