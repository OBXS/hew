#ifndef _AUDIO_DATA_H_
#define _AUDIO_DATA_H_

#include <Windows.h>
#include <XAudio2.h>
#include "StringExtension.h"

struct AudioData {
	string name;
	int loop;
	BYTE *data;
	DWORD size;
	IXAudio2SourceVoice *sourceVoice;

	~AudioData() {
		delete data;
		sourceVoice->DestroyVoice();
	}
};

#endif