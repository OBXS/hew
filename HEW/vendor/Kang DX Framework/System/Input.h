#ifndef __INPUT_H__
#define __INPUT_H__

#include <Windows.h>
#include <XInput.h>
#pragma comment(lib,"xinput.lib")
#include "InputButton.h"
#include "MathExtension.h"

#define	MAX_KEY	256
#define MAX_CONTROLLER 4
#define CONTROLLER_MAX_BUTTON 24

class Input {
public:
	// Init & Update

	static void Init();
	static void UpdateKeyDown(int key);
	static void UpdateKeyUp(int key);
	static void UpdateMouseMove(POINTS pos);
	static void UpdateMouseDown(MouseButton button);
	static void UpdateMouseUp(MouseButton button);
	static void UpdateMouseWheel(int dir);
	static void UpdateXInput();
	
	// Keyboard
	
	static bool IsKeyPressed(int key);
	static bool IsKeyDown(int key);
	static bool IsKeyUp(int key);
	
	// Mouse
	
	static Vector2 GetMousePos();
	static bool IsMousePressed(MouseButton button);
	static bool IsMouseDown(MouseButton button);
	static bool IsMouseUp(MouseButton button);
	static bool IsMouseWheelScrolledUp();
	static bool IsMouseWheelScrolledDown();
	
	// Controller
	
	static bool IsButtonPressed(InputButton button);
	static bool IsButtonPressed(InputButton button, int no);
	static bool IsButtonDown(InputButton button);
	static bool IsButtonDown(InputButton button, int no);
	static bool IsButtonUp(InputButton button);
	static bool IsButtonUp(InputButton button, int no);
	static float GetInputValue(InputButton button);
	static float GetInputValue(InputButton button, int no);

private:
	// Keyboard

	static bool	keyState[MAX_KEY];
	static bool	keyDown[MAX_KEY];
	static bool	keyUp[MAX_KEY];

	// Mouse

	static POINTS mousePos;
	static bool mouseState[3];
	static bool mouseDown[3];
	static bool mouseUp[3];
	static int mouseWheel;

	// Controller

	struct ControllerState {
		XINPUT_STATE state;
		bool isConnected;
	};
	static ControllerState controllersState[MAX_CONTROLLER];
	static bool controllerUp[MAX_CONTROLLER][CONTROLLER_MAX_BUTTON];
	static bool controllerDown[MAX_CONTROLLER][CONTROLLER_MAX_BUTTON];
};

#endif