#include <Windows.h>
#include "Global.h"
#include "ScreenFader.h"
#include "..\Game.h"

#define CLASS_NAME "GameWindow"
#define WINDOW_TITTLE "Library"

bool Init(HINSTANCE hInstance, int nCmdShow);
HWND InitWindow(HINSTANCE hInstance, int nCmdShow);
void InitRneder();
LRESULT CALLBACK WndProc(HWND g_hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

void Update();
void Draw();

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	if (!Init(hInstance, nCmdShow)) return 0;

	MSG msg = {};
	while (WM_QUIT != msg.message) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else {
			Update();
			Draw();
			Time::WaitFrame();
		}
	}
	
	return (int)msg.wParam;
}

bool Init(HINSTANCE hInstance, int nCmdShow) {
	IDirect3D9 *d3d9 = Direct3DCreate9(D3D_SDK_VERSION);
	if (!d3d9) {
		MessageBox(NULL, "Failed to initialize DirectX.", "Error", MB_ICONASTERISK);
		return false;
	}

	g_hWnd = InitWindow(hInstance, nCmdShow);

	D3DPRESENT_PARAMETERS d3dpp = {};
	d3dpp.BackBufferWidth = SCREEN_W;
	d3dpp.BackBufferHeight = SCREEN_H;
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;
	
	if (FAILED(d3d9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, g_hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &GetDevice()))) {
		MessageBox(g_hWnd, "Failed to create device.", "Error", MB_ICONASTERISK);
		return false;
	}
	InitRneder();
	
	srand((unsigned)time(nullptr));
	Time::Init();
	Input::Init();
	Audio::Init();
	InitDebugFont();
	InitGame();

	return true;
}

HWND InitWindow(HINSTANCE hInstance, int nCmdShow) {
	WNDCLASS wc = {};
	wc.lpfnWndProc = WndProc;
	wc.hInstance = hInstance;
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_BACKGROUND + 1);
	wc.lpszClassName = CLASS_NAME;
	RegisterClass(&wc);

#ifdef _DEBUG
	RECT windowRect = { 0, 0, SCREEN_W, SCREEN_H };
	DWORD windowStyle = WS_OVERLAPPEDWINDOW ^ (WS_THICKFRAME | WS_MAXIMIZEBOX);
	AdjustWindowRect(&windowRect, windowStyle, FALSE);
#else
	RECT windowRect = { 0, 0, GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN) };
	DWORD windowStyle = WS_POPUP;
#endif
	POINT windowSize = { windowRect.right - windowRect.left, windowRect.bottom - windowRect.top };
	POINT windowPos = { max((GetSystemMetrics(SM_CXSCREEN) - windowSize.x) / 2, 0), max((GetSystemMetrics(SM_CYSCREEN) - windowSize.y) / 2, 0) };
	HWND g_hWnd = CreateWindow(CLASS_NAME, WINDOW_TITTLE, windowStyle,
							 windowPos.x, windowPos.y, windowSize.x, windowSize.y,
							 NULL, NULL, hInstance, NULL);
	ShowWindow(g_hWnd, nCmdShow);
	UpdateWindow(g_hWnd);
	return g_hWnd;
}

void InitRneder() {
	// Mode
	GetDevice()->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1);
	GetDevice()->SetRenderState(D3DRS_LIGHTING, false);

	// Cull
	GetDevice()->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

	// Sampler
	GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);

	// Z-buffer
	GetDevice()->SetRenderState(D3DRS_ZENABLE, TRUE);

	// Alpha Test
	GetDevice()->SetRenderState(D3DRS_ALPHAREF, (DWORD)0x00000001);
	GetDevice()->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
	GetDevice()->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL);

	// Alpha Blend
	GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// Texture Alpha
	GetDevice()->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	GetDevice()->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	GetDevice()->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

	// WVP Matrix
	Matrix mWVP;
	D3DXMatrixLookAtLH(&mWVP, &Vector3(0.f, 0.f, -1500.f), &Vector3(0.f, 0.f, 0.f), &Vector3(0.f, 1.f, 0.f));
	GetDevice()->SetTransform(D3DTS_VIEW, &mWVP);
	D3DXMatrixOrthoLH(&mWVP, SCREEN_W, SCREEN_H, 0.f, 3000.f);
	GetDevice()->SetTransform(D3DTS_PROJECTION, &mWVP);
}

LRESULT CALLBACK WndProc(HWND g_hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
	case WM_KEYDOWN:
		if (wParam == VK_ESCAPE)
			SendMessage(g_hWnd, WM_CLOSE, 0, 0);
		Input::UpdateKeyDown(wParam);
		break;

	case WM_KEYUP:
		Input::UpdateKeyUp(wParam);
		break;

	case WM_MOUSEMOVE:
		Input::UpdateMouseMove(MAKEPOINTS(lParam));
		break;

	case WM_LBUTTONDOWN:
		Input::UpdateMouseDown(MouseButton::Left);
		break;

	case WM_LBUTTONUP:
		Input::UpdateMouseUp(MouseButton::Left);
		break;

	case WM_RBUTTONDOWN:
		Input::UpdateMouseDown(MouseButton::Right);
		break;

	case WM_RBUTTONUP:
		Input::UpdateMouseUp(MouseButton::Right);
		break;

	case WM_MBUTTONDOWN:
		Input::UpdateMouseDown(MouseButton::Middle);
		break;

	case WM_MBUTTONUP:
		Input::UpdateMouseUp(MouseButton::Middle);
		break;

	case WM_MOUSEWHEEL:
		Input::UpdateMouseWheel(GET_WHEEL_DELTA_WPARAM(wParam));
		break;

	case WM_CLOSE:
		if (MessageBox(g_hWnd, "Do you really want to quit game?\n\n本\x93\x96に終了してよろしいですか？", "Quit Game / 終了", MB_OKCANCEL | MB_DEFBUTTON2) == IDOK)
			DestroyWindow(g_hWnd);
		return 0;

	case WM_DESTROY:
		Audio::Fin();
		FinDebugFont();
		PostQuitMessage(0);
		return 0;
	}

	return DefWindowProc(g_hWnd, uMsg, wParam, lParam);
}

void Update() {
	Input::UpdateXInput();
	UpdateGame();
}

void Draw() {
	GetDevice()->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, Color(1.f, 1.f, 1.f, 1.f), 1.f, 0);
	GetDevice()->BeginScene();

	DrawGame();
	ScreenFader::Draw();

	GetDevice()->EndScene();
	GetDevice()->Present(NULL, NULL, NULL, NULL);
}