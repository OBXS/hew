#include "Audio.h"

IXAudio2 *Audio::xAudio2 = nullptr;
IXAudio2MasteringVoice *Audio::masteringVoice = nullptr;
map<string, AudioData*> *Audio::audioList = new map<string, AudioData*>;

void Audio::Init() {
	CoInitializeEx(NULL, COINIT_MULTITHREADED);
	if (FAILED(XAudio2Create(&xAudio2, 0))) {
		ErrorMessage("Failed to creat \"XAudio2.\"");
		return;
	}
	if (FAILED(xAudio2->CreateMasteringVoice(&masteringVoice))) {
		ErrorMessage("Failed to creat \"XAudio2 Mastering Voice.\"");
		return;
	}
}

void Audio::Fin() {
	audioList->clear();
	delete audioList;
	masteringVoice->DestroyVoice();
	xAudio2->Release();
	CoUninitialize();
}

void Audio::Load(string fileName, int loop) {
	string audioName = fileName;
	RemoveExtension(audioName);
	if (audioList->find(audioName) != audioList->end()) return;

	AudioData *audioData = new AudioData();
	audioData->name = audioName;
	audioData->loop = loop;
	string pathName = "Resources/Audio/";
	pathName.append(fileName);

	HANDLE file = nullptr;
	WAVEFORMATEXTENSIBLE wfx;
	XAUDIO2_BUFFER buffer;
	DWORD chunkSize = 0;
	DWORD chunkPosition = 0;
	DWORD filetype = 0;
	memset(&wfx, 0, sizeof(WAVEFORMATEXTENSIBLE));
	memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));

	file = CreateFile(pathName.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (file == INVALID_HANDLE_VALUE) {
		ErrorMessage("Failed to creat audio file with ", fileName);
		return;
	}
	if (SetFilePointer(file, 0, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER) {
		ErrorMessage("Failed to creat audio file with ", fileName);
		return;
	}

	// Check WAVE File
	if (FAILED(CheckChunk(file , 'FFIR', &chunkSize, &chunkPosition))) {
		ErrorMessage("Failed to load wav file ", fileName);
		return;
	}
	if (FAILED(ReadChunkData(file, &filetype, sizeof(DWORD), chunkPosition))) {
		ErrorMessage("Failed to check wav file ", fileName);
		return;
	}
	if(filetype != 'EVAW') {
		ErrorMessage("Failed to check wav file ", fileName);
		return;
	}

	// Check Format
	if(FAILED(CheckChunk(file, ' tmf', &chunkSize, &chunkPosition))) {
		ErrorMessage("Failed to check format wiht ", fileName);
		return;
	}
	if(FAILED(ReadChunkData(file, &wfx, chunkSize, chunkPosition))) {
		ErrorMessage("Failed to check format with ", fileName);
		return;
	}

	// Load File
	if(FAILED(CheckChunk(file, 'atad', &audioData->size, &chunkPosition))) {
		ErrorMessage("Failed to load audio file ", fileName);
		return;
	}
	audioData->data = (BYTE*)malloc(audioData->size);
	if(FAILED(ReadChunkData(file, audioData->data, audioData->size, chunkPosition))) {
		ErrorMessage("Failed to load audio file ", fileName);
		return;
	}

	// Creat Source Voice
	if(FAILED(xAudio2->CreateSourceVoice(&audioData->sourceVoice, &(wfx.Format)))) {
		ErrorMessage("Failed to creat source voice with ", fileName);
		return;
	}

	(*audioList)[audioName] = audioData;
}

void Audio::Release(string name) {
	RemoveExtension(name);
	if (audioList->find(name) == audioList->end()) {
		ErrorMessage("Didn't load audio \"", name);
	} else {
		audioList->erase(name);
	}
}

#pragma region Action

void Audio::Play(string name) {
	RemoveExtension(name);
	if (audioList->find(name) == audioList->end()) {
		ErrorMessage("Didn't load audio \"", name);
	} else {
		XAUDIO2_VOICE_STATE xa2state;
		(*audioList)[name]->sourceVoice->GetState(&xa2state);

		if (!xa2state.BuffersQueued) {
			XAUDIO2_BUFFER buffer;
			memset(&buffer, 0, sizeof(XAUDIO2_BUFFER));
			buffer.AudioBytes = (*audioList)[name]->size;
			buffer.pAudioData = (*audioList)[name]->data;
			buffer.Flags = XAUDIO2_END_OF_STREAM;
			buffer.LoopCount = (*audioList)[name]->loop;
			(*audioList)[name]->sourceVoice->SubmitSourceBuffer(&buffer);
		}
		(*audioList)[name]->sourceVoice->Start(0);
	}
}

void Audio::PlayAll() {
	for (auto &audio : *audioList) {
		XAUDIO2_VOICE_STATE xa2state;
		audio.second->sourceVoice->GetState(&xa2state);
		if (xa2state.BuffersQueued)
			audio.second->sourceVoice->Start(0);
	}
}

void Audio::Pause(string name) {
	RemoveExtension(name);
	if (audioList->find(name) == audioList->end()) {
		ErrorMessage("Didn't load audio \"", name);
	} else {
		XAUDIO2_VOICE_STATE xa2state;
		(*audioList)[name]->sourceVoice->GetState(&xa2state);
		if (xa2state.BuffersQueued)
			(*audioList)[name]->sourceVoice->Stop(0);
	}
}

void Audio::PauseAll() {
	for (auto &audio : *audioList) {
		XAUDIO2_VOICE_STATE xa2state;
		audio.second->sourceVoice->GetState(&xa2state);
		if (xa2state.BuffersQueued)
			audio.second->sourceVoice->Stop(0);
	}
}

void Audio::Stop(string name) {
	RemoveExtension(name);
	if (audioList->find(name) == audioList->end()) {
		ErrorMessage("Didn't load audio \"", name);
	} else {
		XAUDIO2_VOICE_STATE xa2state;
		(*audioList)[name]->sourceVoice->GetState(&xa2state);
		if (xa2state.BuffersQueued) {
			(*audioList)[name]->sourceVoice->Stop(0);
			(*audioList)[name]->sourceVoice->FlushSourceBuffers();
		}
	}
}

void Audio::StopAll() {
	for (auto &audio : *audioList) {
		XAUDIO2_VOICE_STATE xa2state;
		audio.second->sourceVoice->GetState(&xa2state);
		if (xa2state.BuffersQueued) {
			audio.second->sourceVoice->Stop(0);
			audio.second->sourceVoice->FlushSourceBuffers();
		}
	}
}

// Action
#pragma endregion

HRESULT Audio::CheckChunk(HANDLE hFile, DWORD format, DWORD *pChunkSize, DWORD *pChunkDataPosition) {
	HRESULT hr = S_OK;
	DWORD dwRead;
	DWORD dwChunkType;
	DWORD dwChunkDataSize;
	DWORD dwRIFFDataSize = 0;
	DWORD dwFileType;
	DWORD dwBytesRead = 0;
	DWORD dwOffset = 0;
	
	if(SetFilePointer(hFile, 0, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{// ファイルポインタを先頭に移動
		return HRESULT_FROM_WIN32(GetLastError());
	}
	
	while(hr == S_OK)
	{
		if(ReadFile(hFile, &dwChunkType, sizeof(DWORD), &dwRead, NULL) == 0)
		{// チャンクの読み込み
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		if(ReadFile(hFile, &dwChunkDataSize, sizeof(DWORD), &dwRead, NULL) == 0)
		{// チャンクデータの読み込み
			hr = HRESULT_FROM_WIN32(GetLastError());
		}

		switch(dwChunkType)
		{
		case 'FFIR':
			dwRIFFDataSize  = dwChunkDataSize;
			dwChunkDataSize = 4;
			if(ReadFile(hFile, &dwFileType, sizeof(DWORD), &dwRead, NULL) == 0)
			{// ファイルタイプの読み込み
				hr = HRESULT_FROM_WIN32(GetLastError());
			}
			break;

		default:
			if(SetFilePointer(hFile, dwChunkDataSize, NULL, FILE_CURRENT) == INVALID_SET_FILE_POINTER)
			{// ファイルポインタをチャンクデータ分移動
				return HRESULT_FROM_WIN32(GetLastError());
			}
		}

		dwOffset += sizeof(DWORD) * 2;
		if(dwChunkType == format)
		{
			*pChunkSize         = dwChunkDataSize;
			*pChunkDataPosition = dwOffset;

			return S_OK;
		}

		dwOffset += dwChunkDataSize;
		if(dwBytesRead >= dwRIFFDataSize)
		{
			return S_FALSE;
		}
	}
	
	return S_OK;
}

HRESULT Audio::ReadChunkData(HANDLE hFile, void *pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset) {
	DWORD dwRead;
	
	if(SetFilePointer(hFile, dwBufferoffset, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{// ファイルポインタを指定位置まで移動
		return HRESULT_FROM_WIN32(GetLastError());
	}

	if(ReadFile(hFile, pBuffer, dwBuffersize, &dwRead, NULL) == 0)
	{// データの読み込み
		return HRESULT_FROM_WIN32(GetLastError());
	}
	
	return S_OK;
}