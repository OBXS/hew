#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "Transform.h"

class Camera : public Transform {
public:
	Camera();

	void Update();

	Vector2 InputToCamera(Vector2 input);

private:
	Vector3 eye;
	Vector3 at;
	Vector3 up;

	Sequence *debugAnim = nullptr;
};

#endif