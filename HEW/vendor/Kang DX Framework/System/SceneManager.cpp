// SceneManager.cpp
// シーンマネージャー
//
// Manage scene objects. Call the functions by class name.
// シーンオブジェクトを管理する。クラス名から関数を呼び出す。

#include "SceneManager.h"

map<string, Scene*> *SceneManager::sceneList = new map<string, Scene*>();
Scene *SceneManager::scene = nullptr;
string SceneManager::name = "";

void SceneManager::AddScene(string name, Scene *scene) {
	if (sceneList->find(name) != sceneList->end()) return;

	(*sceneList)[name] = scene;
}

void SceneManager::RemoveScene(string name) {
	if (sceneList->find(name) != sceneList->end()) return;

	sceneList->erase(name);
}

void SceneManager::ChangeScene(string name) {
	if (sceneList->find(name) == sceneList->end()) return;

	if (scene)
		scene->Exit();

	scene = (*sceneList)[name];
	SceneManager::name = name;

	if (scene)
		scene->Enter();
}

Scene *SceneManager::GetNowScene() {
	return scene;
}

string SceneManager::GetNowSceneName() {
	return name;
}

void SceneManager::Update() {
	if (scene)
		scene->Update();
}

void SceneManager::Draw() {
	if (scene)
		scene->Draw();
}