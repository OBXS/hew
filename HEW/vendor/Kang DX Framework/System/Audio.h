#ifndef _AUDIO_H_
#define _AUDIO_H_

#include <map>
#include "AudioData.h"

class Audio {
public:
	static void Init();
	static void Fin();
	
	static void Load(string fileName, int loop);
	static void Release(string name);

	static void Play(string name);
	static void PlayAll();
	static void Pause(string name);
	static void PauseAll();
	static void Stop(string name);
	static void StopAll();

private:
	static IXAudio2 *xAudio2;
	static IXAudio2MasteringVoice *masteringVoice;
	static map<string, AudioData*> *audioList;

	static HRESULT CheckChunk(HANDLE hFile, DWORD format, DWORD *pChunkSize, DWORD *pChunkDataPosition);
	static HRESULT ReadChunkData(HANDLE hFile, void *pBuffer, DWORD dwBuffersize, DWORD dwBufferoffset);
};

#endif