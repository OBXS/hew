#include <WIndows.h>
#include <stdio.h>
#include "Debug.h"

static LPD3DXFONT font = nullptr;

void DebugPrintf(int n) {
	DebugPrintf("%d\n", n);
}

void DebugPrintf(float f) {
	DebugPrintf("%f\n", f);
}

void DebugPrintf(Vector2 v) {
	DebugPrintf("Vector2(%f, %f)\n", v.x, v.y);
}

void DebugPrintf(Vector3 v) {
	DebugPrintf("Vector3(%f, %f, %f)\n", v.x, v.y, v.z);
}

void DebugPrintf(Matrix m) {
	DebugPrintf("Matrix4x4(\n1 %f %f %f %f\n", m._11, m._21, m._31, m._41);
	DebugPrintf("2 %f %f %f %f\n", m._12, m._22, m._32, m._42);
	DebugPrintf("3 %f %f %f %f\n", m._13, m._23, m._33, m._43);
	DebugPrintf("4 %f %f %f %f)\n", m._14, m._24, m._34, m._44);
}

void DebugPrintf(const char *pFormat, ...) {
#if defined(_DEBUG) || defined(DEBUG)
	va_list argp;
	char buf[256];

	va_start(argp, pFormat);
	vsprintf_s(buf, 256, pFormat, argp);
	va_end(argp);
	OutputDebugString(buf);
#endif // _DEBUG || DEBUG
}

void InitDebugFont(void) {
#if defined(_DEBUG) || defined(DEBUG)
	D3DXCreateFont(GetDevice(),
		           24, 0, 0, 0,
		           FALSE,
		           SHIFTJIS_CHARSET,
		           OUT_DEFAULT_PRECIS,
		           DEFAULT_QUALITY,
		           DEFAULT_PITCH,
		           "Arial", 
		           &font);
#endif // _DEBUG || DEBUG
}

void FinDebugFont(void) {
#if defined(_DEBUG) || defined(DEBUG)
	font->Release();
#endif // _DEBUG || DEBUG
}

void DebugFontDraw(int x, int y, const char* pFormat, ...) {
#if defined(_DEBUG) || defined(DEBUG)
	RECT rect = { x, y, SCREEN_W, SCREEN_H };
	va_list argp;
	char buf[256];

	va_start(argp, pFormat);
	vsprintf_s(buf, 256, pFormat, argp);
	va_end(argp);

	font->DrawText(NULL, buf, -1, &rect, DT_LEFT, D3DCOLOR_RGBA(0, 255, 0, 255));
#endif // _DEBUG || DEBUG
}