#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include <time.h>
#include <d3dx9.h>
#include "Time.h"
#include "Input.h"
#include "Audio.h"
#include "MathExtension.h"
#include "StringExtension.h"
#include "Debug.h"

#define SCREEN_W 1920
#define SCREEN_H 1080

static LPDIRECT3DDEVICE9 g_device = nullptr;

inline LPDIRECT3DDEVICE9 &GetDevice() {
	return g_device;
}

#endif