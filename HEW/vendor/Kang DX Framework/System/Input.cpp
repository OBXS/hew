#include "Input.h"

#define TRIGGER_MAX (0x00FF)
#define STICK_MAX (0x7FFF)
#define TRIGGER_DEADZONE (int)(0.24f * TRIGGER_MAX)
#define STICK_DEADZONE (int)(0.24f * STICK_MAX)

// Keyboard

bool Input::keyState[MAX_KEY];
bool Input::keyDown[MAX_KEY];
bool Input::keyUp[MAX_KEY];

// Mouse

POINTS Input::mousePos;
bool Input::mouseState[3];
bool Input::mouseDown[3];
bool Input::mouseUp[3];
int Input::mouseWheel = 0;

// Controller

Input::ControllerState Input::controllersState[MAX_CONTROLLER];
bool Input::controllerUp[MAX_CONTROLLER][CONTROLLER_MAX_BUTTON] = { 0 };
bool Input::controllerDown[MAX_CONTROLLER][CONTROLLER_MAX_BUTTON] = { 0 };

#pragma region Init & Update

void Input::Init() {
	ZeroMemory(controllersState, sizeof(ControllerState) * MAX_CONTROLLER);
}

void Input::UpdateKeyDown(int key) {
	if (keyState[key]) {
		keyDown[key] = false;
	} else {
		keyState[key] = true;
		keyDown[key] = true;
		keyUp[key] = false;
	}
}

void Input::UpdateKeyUp(int key) {
	if (keyState[key]) {
		keyState[key] = false;
		keyUp[key] = true;
		keyDown[key] = false;
	} else {
		keyUp[key] = false;
	}
}

void Input::UpdateMouseMove(POINTS pos) {
	mousePos = pos;
}

void Input::UpdateMouseDown(MouseButton button) {
	if (mouseState[(int)button]) {
		mouseDown[(int)button] = false;
	} else {
		mouseState[(int)button] = true;
		mouseDown[(int)button] = true;
		mouseUp[(int)button] = false;
	}
}

void Input::UpdateMouseUp(MouseButton button) {
	if (mouseState[(int)button]) {
		mouseState[(int)button] = false;
		mouseUp[(int)button] = true;
		mouseDown[(int)button] = false;
	} else {
		mouseUp[(int)button] = false;
	}
}

void Input::UpdateMouseWheel(int dir) {
	mouseWheel = dir;
}

void Input::UpdateXInput() {
	DWORD dwResult;
	for (int i = 0; i < MAX_CONTROLLER; i++) {
		dwResult = XInputGetState(i, &controllersState[i].state);

		controllersState[i].isConnected = (XInputGetState(i, &controllersState[i].state) == ERROR_SUCCESS);
	}
}

// Init & Update
#pragma endregion

#pragma region Keyboard

bool Input::IsKeyPressed(int key) {
	return keyState[key];
}

bool Input::IsKeyDown(int key) {
	if (keyDown[key]) {
		keyDown[key] = false;
		return true;
	}
	return false;
}

bool Input::IsKeyUp(int key) {
	if (keyUp[key]) {
		keyUp[key] = false;
		return true;
	}
	return false;
}

#pragma endregion

#pragma region Mouse

Vector2 Input::GetMousePos() {
	return Vector2(mousePos.x, mousePos.y);
}

bool Input::IsMousePressed(MouseButton button) {
	return mouseState[(int)button];
}

bool Input::IsMouseDown(MouseButton button) {
	if (mouseDown[(int)button]) {
		mouseDown[(int)button] = false;
		return true;
	}
	return false;
}

bool Input::IsMouseUp(MouseButton button) {
	if (mouseUp[(int)button]) {
		mouseUp[(int)button] = false;
		return true;
	}
	return false;
}

bool Input::IsMouseWheelScrolledUp() {
	if (mouseWheel > 0) {
		mouseWheel = 0;
		return true;
	}
	return false;
}

bool Input::IsMouseWheelScrolledDown() {
	if (mouseWheel < 0) {
		mouseWheel = 0;
		return true;
	}
	return false;
}

// Mouse
#pragma endregion

#pragma region Controller

bool Input::IsButtonPressed(InputButton button) {
	return IsButtonPressed(button, 0);
}

bool Input::IsButtonPressed(InputButton button, int no) {
	if (no < 0 || no >= MAX_CONTROLLER) return false;
	if (!controllersState[no].isConnected) return false;

	if (button < InputButton::TriggerLeft) {
		return (controllersState[no].state.Gamepad.wButtons & (int)button);
	} else {
		switch (button) {
		case InputButton::TriggerLeft:
			return (controllersState[no].state.Gamepad.bLeftTrigger > TRIGGER_DEADZONE);
		case InputButton::TriggerRight:
			return (controllersState[no].state.Gamepad.bRightTrigger > TRIGGER_DEADZONE);
		case InputButton::StickLeftUp:
			return (controllersState[no].state.Gamepad.sThumbLY > STICK_DEADZONE);
		case InputButton::StickLeftDown:
			return (controllersState[no].state.Gamepad.sThumbLY < -STICK_DEADZONE);
		case InputButton::StickLeftLeft:
			return (controllersState[no].state.Gamepad.sThumbLX < -STICK_DEADZONE);
		case InputButton::StickLeftRight:
			return (controllersState[no].state.Gamepad.sThumbLX > STICK_DEADZONE);
		case InputButton::StickRightUp:
			return (controllersState[no].state.Gamepad.sThumbRY > STICK_DEADZONE);
		case InputButton::StickRightDown:
			return (controllersState[no].state.Gamepad.sThumbRY < -STICK_DEADZONE);
		case InputButton::StickRightLeft:
			return (controllersState[no].state.Gamepad.sThumbRX < -STICK_DEADZONE);
		case InputButton::StickRightRight:
			return (controllersState[no].state.Gamepad.sThumbRX > STICK_DEADZONE);
		default:
			return false;
		}
	}
}

bool Input::IsButtonDown(InputButton button) {
	return IsButtonDown(button, 0);
}

bool Input::IsButtonDown(InputButton button, int no) {
	if (no < 0 || no >= MAX_CONTROLLER) return false;
	if (!controllersState[no].isConnected) return false;

	int b = (int)log2f((float)button);
	if (controllerUp[no][b]) {
		if (!IsButtonPressed(button, no))
			controllerUp[no][b] = false;
		return false;
	} else {
		if (IsButtonPressed(button, no)) {
			return controllerUp[no][b] = true;
		} else {
			return false;
		}
	}
}

bool Input::IsButtonUp(InputButton button) {
	return IsButtonUp(button, 0);
}

bool Input::IsButtonUp(InputButton button, int no) {
	if (no < 0 || no >= MAX_CONTROLLER) return false;
	if (!controllersState[no].isConnected) return false;

	int b = (int)log2f((float)button);
	if (controllerDown[no][b]) {
		if (IsButtonPressed(button, no))
			controllerDown[no][b] = false;
		return false;
	} else {
		if (!IsButtonPressed(button, no)) {
			return controllerDown[no][b] = true;
		} else {
			return false;
		}
	}
}

float Input::GetInputValue(InputButton button) {
	return GetInputValue(button, 0);
}

float Input::GetInputValue(InputButton button, int no) {
	if (no < 0 || no >= MAX_CONTROLLER) return 0.f;
	if (!controllersState[no].isConnected) return 0.f;

	if (button < InputButton::TriggerLeft) {
		return (controllersState[no].state.Gamepad.wButtons & (int)button) ? 1.f : 0.f;
	} else {
		float value = 0.f;
		switch (button) {
		case InputButton::TriggerLeft:
			value = controllersState[no].state.Gamepad.bLeftTrigger;
			return  (value > TRIGGER_DEADZONE) ? value / TRIGGER_MAX : 0.f;
		case InputButton::TriggerRight:
			value = controllersState[no].state.Gamepad.bRightTrigger;
			return (value > TRIGGER_DEADZONE) ? value / TRIGGER_MAX : 0.f;
		case InputButton::StickLeftUp:
		case InputButton::StickLeftDown:
			value = controllersState[no].state.Gamepad.sThumbLY;
			return (value < -STICK_DEADZONE || value > STICK_DEADZONE) ? value / STICK_MAX : 0.f;
		case InputButton::StickLeftLeft:
		case InputButton::StickLeftRight:
			value = controllersState[no].state.Gamepad.sThumbLX;
			return (value < -STICK_DEADZONE || value > STICK_DEADZONE) ? value / STICK_MAX : 0.f;
		case InputButton::StickRightUp:
		case InputButton::StickRightDown:
			value = controllersState[no].state.Gamepad.sThumbRY;
			return (value < -STICK_DEADZONE || value > STICK_DEADZONE) ? value / STICK_MAX : 0.f;
		case InputButton::StickRightLeft:
		case InputButton::StickRightRight:
			value = controllersState[no].state.Gamepad.sThumbRX;
			return (value < -STICK_DEADZONE || value > STICK_DEADZONE) ? value / STICK_MAX : 0.f;
		default:
			return value;
		}
	}
}

// Controller
#pragma endregion