#include "ScreenFader.h"

Rect *ScreenFader::image = new Rect(Vector3(0.f, 0.f, -1499.f), Vector2(SCREEN_W, SCREEN_H), Color(0.f, 0.f, 0.f, 0.f));
float ScreenFader::timer = 0.f;
float ScreenFader::duration = 0.f;
Color ScreenFader::colorStart = Color(0.f, 0.f, 0.f, 0.f);
Color ScreenFader::colorEnd = Color(0.f, 0.f, 0.f, 0.f);

void ScreenFader::Draw() {
	timer += Time::GetDeltaTime();
	float rate = (duration == 0.f) ? 1.f : timer / duration;
	Color color;
	D3DXColorLerp(&color, &colorStart, &colorEnd, rate);
	
	if (color.a <= 0.f) return;

	image->SetColor(color);
	image->Draw();
}

void ScreenFader::Fade(float duration, Color start, Color end) {
	timer = 0.f;
	ScreenFader::duration = duration;
	colorStart = start;
	colorEnd = end;
}

bool ScreenFader::IsActive() {
	if (duration == 0.f) return false;
	return (timer <= duration) ? true : false;
}

void ScreenFader::SetCamera(Transform *camera) {
	image->SetParent(camera);
}