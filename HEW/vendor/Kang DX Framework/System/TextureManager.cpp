// TextureManager.cpp
// テクスチャマネージャー
//
// Manage textures. Call the functions by class name.
// テクスチャを管理する。クラス名から関数を呼び出す。

#include "TextureManager.h"

map<string, Texture*> *TextureManager::textureList = new map<string, Texture*>();

Texture *TextureManager::Load(string fileName) {
	return Load(fileName, Vector2(0.f, 0.f), true);
}

Texture *TextureManager::Load(string fileName, Vector2 size) {
	return Load(fileName, size, false);
}

Texture *TextureManager::Load(string fileName, Vector2 size, bool isPOTSize) {
	string textureName = fileName;
	RemoveExtension(textureName);
	if (textureList->find(textureName) != textureList->end()) return nullptr;

	Texture *texture = new Texture();
	string pathName = "Resources/Textures/";
	pathName.append(fileName);

	// Load File
	if (FAILED(D3DXCreateTextureFromFile(GetDevice(), pathName.c_str(), &(texture->texture)))) {
		ErrorMessage("Failed to load image \"", fileName);
		return nullptr;
	}

	// Load Data
	texture->name = textureName;
	if (isPOTSize) {
		D3DSURFACE_DESC desc;
		texture->texture->GetLevelDesc(NULL, &desc);
		texture->size = Vector2((float)desc.Width, (float)desc.Height);
	} else {
		texture->size = size;
	}
	(*textureList)[textureName] = texture;

	return (*textureList)[textureName];
}

Texture *TextureManager::Get(string name) {
	RemoveExtension(name);
	if (textureList->find(name) == textureList->end()) {
		ErrorMessage("Didn't load image \"", name);
		return nullptr;
	} else {
		return (*textureList)[name];
	}
}

void TextureManager::Release(string name) {
	RemoveExtension(name);
	if (textureList->find(name) == textureList->end()) {
		ErrorMessage("Didn't load image \"", name);
	} else {
		textureList->erase(name);
	}
}