#ifndef __TIME_H__
#define __TIME_H__

#include <Windows.h>
#pragma comment(lib,"winmm")

class Time {
public:
	static void Init();
	static void WaitFrame();
	static float GetTime();
	static float GetDeltaTime();

private:
	static double frequency;
	static LARGE_INTEGER nowTime;
	static LARGE_INTEGER lastTime;
	static float deltaTime;
};

#endif