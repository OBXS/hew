// ObjectPool.h
// オブジェクトプール
//
// Auto creating and recyclable container. Set type with no parameters constructer to template.
// 自動的に生成と再利用できる容器。パラメータなしコンストラクタ持ち型をテンプレートに設定して使う。

#ifndef __OBJECT_POOL_H__
#define __OBJECT_POOL_H__

#include <list>

using namespace std;

template <class T>
class ObjectPool {
public:
	struct PoolingObject {
	public:
		T *object;
		bool isActive = false;

		PoolingObject(T *object) {
			this->object = object;
		}

		~PoolingObject() {
			delete object;
		}
	};

	ObjectPool(int size = 0, bool isGrowable = true) {
		this->size = size;
		this->isGrowable = isGrowable;
		objects = new list<PoolingObject*>();

		if (size <= 0) return;
		for (int i = 0; i < size; i++)
			objects->push_back(new PoolingObject(new T()));
	}

	~ObjectPool() {
		delete objects;
	}

	T *Get() {
		for (auto &object : *objects) {
			if (!object->isActive) {
				object->isActive = true;
				return object->object;
			}
		}

		if (size < (int)objects->size() || isGrowable) {
			objects->push_back(new PoolingObject(new T()));
			objects->back()->isActive = true;
			return objects->back()->object;
		}

		return nullptr;
	}

	void Putback(T *backObject) {
		for (auto &object : *objects) {
			if (object->object == backObject) {
				object->isActive = false;
			}
		}
	}

protected:
	int size;
	bool isGrowable = false;
	list<PoolingObject*> *objects;
};

#endif