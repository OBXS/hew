#include "SampleScene.h"
#include "System\ScreenFader.h"

SampleScene::SampleScene(Camera *camera) {
	this->camera = camera;
	player = new Player();
}

SampleScene::~SampleScene() {
	delete player;
}

void SampleScene::Enter() {
	ScreenFader::Fade(1.f, Color(0.f, 0.f, 0.f, 1.f), Color(0.f, 0.f, 0.f, 0.f));
}

void SampleScene::Update() {
	player->Update();
}

void SampleScene::Draw() {
	player->Draw();
}

void SampleScene::Exit() {}