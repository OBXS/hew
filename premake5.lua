solution "World Jump"
    architecture "x64"
    startproject "Sandbox"

    configurations
    {
        "Debug",
        "Release",
        "Distribution"
    }

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

-- Include directories relative to root folder (solution directory)
IncludeDir = {}

project "HEW"
    location "HEW"
    kind "WindowedApp"
    language "C++"
    staticruntime "off"
    characterset ("ASCII")

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    files
    {
        -- Add all source and header files
        "%{prj.name}/src/**.h",
        "%{prj.name}/src/**.cpp",

        -- Also include Files for dependencies
        "%{prj.name}/vendor/Kang DX Framework/**.h",
        "%{prj.name}/vendor/Kang DX Framework/**.cpp"
    }

    includedirs
    {
        "%{prj.name}/src",
        "%{prj.name}/vendor/Kang DX Framework",
        "%{prj.name}/vendor/DX9/Include",
    }

    libdirs 
    {
        "%{prj.name}/vendor/DX9/Lib/x64/"
    }

    links
    {
        "d3d9.lib"
    }

    defines
    {
        "_CRT_SECURE_NO_WARNINGS",
        "_HAS_STD_BYTE=0"
    }

    filter "system:windows"
        cppdialect "C++17"
        systemversion "latest"

    filter "configurations:Debug"
        runtime "Debug"
        symbols "On"
    filter { "system:windows", "configurations:Debug" }
        buildoptions "/MDd"
        
        links
        {
            "d3dx9.lib"
        }

    filter "configurations:Release"
        runtime "Release"
        optimize "On"
    filter { "system:windows", "configurations:Release" }
        buildoptions "/MD"
        links
        {
            "d3dx9.lib"
        }

    filter "configurations:Dist"
        runtime "Release"
        optimize "On"
    filter { "system:windows", "configurations:Dist" }
        buildoptions "/MD"
        links
        {
            "d3dx9.lib"
        }